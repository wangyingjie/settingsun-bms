package com.wonu.settingsun.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName PageVo
 * @date 2023/11/7 20:35
 * @description  分页展示类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class PageVo<T> {
    private T data;
    private Long total;
}