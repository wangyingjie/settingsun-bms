package com.wonu.settingsun.domain.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * (AppointmentOrder)实体类
 *
 * @author makejava
 * @since 2023-11-09 16:19:49
 */
@Data
@TableName("t_appointment_order")
public class AppointmentOrder {
    /**
     * 预约订单id
     */
    @TableId(value = "appointment_id")
    private Long appointmentId;
    /**
     * 医生账号
     */    
    @TableField("doctor_name")
    private String doctorName;
    /**
     * 病人账号
     */    
    @TableField("patient_name")
    private String patientName;

    @TableField("content")
    private String content;
    /**
     * 预约时间
     */    
    @TableField("appointment_time")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date appointmentTime;

    @TableField("cost")
    private Double cost;
    /**
     * 0-取消；1-等待；2-完成
     */    
    @TableField("order_status")
    private Integer orderStatus;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;

}
