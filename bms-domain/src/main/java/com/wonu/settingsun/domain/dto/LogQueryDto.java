package com.wonu.settingsun.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogQueryDto
 * @date 2023/11/9 11:12
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class LogQueryDto {

    private String username;
    private Long spendTime;
    private Date optTime;
}