package com.wonu.settingsun.domain.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;

/**
 * (Drug)实体类
 *
 * @author makejava
 * @since 2023-11-11 11:50:17
 */
@Data
@TableName("t_drug")
@NoArgsConstructor
@AllArgsConstructor
public class Drug {

    @TableId(value = "drug_id")
    private Long drugId;

    @NotBlank(message = "药品不能为空")
    @TableField("name")
    private String name;
    /**
     * 描述
     */    
    @TableField("content")
    private String content;
    /**
     * 图片
     */    
    @TableField("image")
    private String image;
        
    @TableField("type")
    @NotBlank(message = "类型不能为空")
    private String type;
    /**
     * 单价
     */    
    @TableField("price")
    @NotNull(message = "价格不能为空")
    @DecimalMin(value = "1.00",message = "价格最低为1.00")
    @DecimalMax(value = "1000.00",message = "价格最多为1000.00")
    @Digits(integer = 2, fraction = 2)
    private BigDecimal price;
    /**
     * 数量
     */
    @NotNull(message = "数量不能为空")
    @Range(min = 1, max = 999, message = "数量在1-999之间")
    @TableField("stock")

    private Integer stock;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
