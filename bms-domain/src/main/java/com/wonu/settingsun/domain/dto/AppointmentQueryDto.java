package com.wonu.settingsun.domain.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotBlank;
import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AppointmentQueryDto
 * @date 2023/11/10 10:26
 * @description  订单动态查询
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AppointmentQueryDto {

    private String doctorName;
    private String patientName;

    private Integer status;

    private Date appointmentTime;
}