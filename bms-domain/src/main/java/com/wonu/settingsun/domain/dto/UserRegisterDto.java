package com.wonu.settingsun.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;

/**
 * @author nameless
 * @version 1.0
 * @ClassName userRegiserDto
 * @date 2023/11/8 18:47
 * @description  注册Dto
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserRegisterDto {

    @NotBlank(message = "用户名不能为空")
    @Pattern(regexp = "^[a-zA-Z0-9_]{3,15}$",message = "账户必须由3-15位字母、数字、下划线组成")
    private String username;

    @NotBlank(message = "密码不能为空")
    // @Pattern(regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d\\S]{6,20}$", message = "密码必须须包含至少一个大写字母、一个小写字母和一个数字,允许使用特殊字符。长度为 6-20 个字符")
    @Length(min = 6, max = 20, message = "密码的长度范围是6-20")
    private String password;

    @NotBlank(message = "密码不能为空")
    @Length(min = 6, max = 20, message = "密码的长度范围是6-20")
    private String passwordRe;

    private String nikeName;
    private BigDecimal balance;
    @NotNull(message = "未分配角色")
    private Long roleId;

}