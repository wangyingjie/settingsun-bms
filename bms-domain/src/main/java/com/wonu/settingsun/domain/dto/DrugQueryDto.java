package com.wonu.settingsun.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DrugQueryDto
 * @date 2023/11/11 16:17
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class DrugQueryDto {

    private String name;
    private String type;
}