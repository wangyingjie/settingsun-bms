package com.wonu.settingsun.domain.entity;

import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * 权限表(Authority)实体类
 *
 * @author makejava
 * @since 2023-11-06 20:35:18
 */
@Data
@TableName("t_authority")
public class Authority {
    /**
     * 编号
     */
    @TableId(value = "authority_id")
    private Long authorityId;
    /**
     * 所属上级
     */    
    @TableField("parent_id")
    private Long parentId;
    /**
     * 权限名称
     */    
    @TableField("pname")
    private String pname;
    /**
     * 菜单路径
     */    
    @TableField("url")
    private String url;
    /**
     * 权限标识
     */    
    @TableField("code")
    private String code;
    /**
     * 1表示一级菜单；2为二级菜单
     */    
    @TableField("level")
    private Integer level;
    /**
     * 排序
     */    
    @TableField("sort")
    private String sort;
    /**
     * 菜单图标
     */    
    @TableField("icon")
    private String icon;

}
