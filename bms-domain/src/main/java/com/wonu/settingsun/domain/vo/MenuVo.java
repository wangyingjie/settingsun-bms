package com.wonu.settingsun.domain.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MenuVo
 * @date 2023/11/6 19:50
 * @description  菜单展示类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuVo {
    //对应权限表里面的权限id
    private Long id;
    //用于封装菜单的层级标识，对应Permission里面的sort
    private String path;
    //用于封装菜单的名称，对应权限类的pname
    private String title;
    //菜单 对应权限的url，一级标题没有url
    private String linkUrl;
    //菜单对应的icon图标
    private String icon;
    //子菜单
    private List<MenuVo> children;
}