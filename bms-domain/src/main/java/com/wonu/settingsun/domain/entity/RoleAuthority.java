package com.wonu.settingsun.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RoleAuthority
 * @date 2023/11/2 11:07
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_role_authority")
public class RoleAuthority {
    @TableId("ra_id")
    private Long raId;
    @TableField("role_id")
    private Long roleId;
    @TableField("authority_id")
    private Long authorityId;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;
    @TableField("create_by")
    private String createBy;
}