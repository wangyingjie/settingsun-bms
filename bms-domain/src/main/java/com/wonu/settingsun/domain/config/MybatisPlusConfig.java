package com.wonu.settingsun.domain.config;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.core.injector.AbstractMethod;
import com.baomidou.mybatisplus.core.injector.DefaultSqlInjector;
import com.baomidou.mybatisplus.core.metadata.TableInfo;
import com.baomidou.mybatisplus.extension.injector.methods.InsertBatchSomeColumn;
import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.baomidou.mybatisplus.extension.plugins.inner.PaginationInnerInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MybatisPlusConfig
 * @date 2023/11/2 11:08
 * @description  mybatis-plus 的 配置类： 配置分页拦截器 和 批量插入
 */
@Slf4j
public class MybatisPlusConfig {
    /**
     * @author nameless
     * @date 2023/10/16 17:14
     * @param
     * @return MybatisPlusInterceptor
     * @description 配置分页拦截器，
     *  1. 创建MybatisPlusInterceptor对象
     *  2. 创建PaginationInnerInterceptor对象，并进行必要的分页设置
     *  3. 将PaginationInnerInterceptor作为参数放入到mybatis拦截器中
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        log.debug("自动装配Mybatis分页拦截器");
        MybatisPlusInterceptor mybatisPlusInterceptor = new MybatisPlusInterceptor();
        PaginationInnerInterceptor paginationInnerInterceptor = new PaginationInnerInterceptor();
        /*设置分页器对应的数据库的类型*/
        paginationInnerInterceptor.setDbType(DbType.MYSQL);
        /*设置最后一页的越界处理，其他设置见官网*/
        paginationInnerInterceptor.setOverflow(true);
        mybatisPlusInterceptor.addInnerInterceptor(paginationInnerInterceptor);
        return mybatisPlusInterceptor;
    }

    /**
     * @author nameless
     * @date 2023/10/17 10:39
     * @param
     * @return DefaultSqlInjector
     * @description
     * MybatisPlus的批量添加解决方案，重写DefaultSqlInjector类的getMethodList方法
     *
     *  1. 调用父类的getMethodList
     *  2. 获取的list对象调用add()添加一个InsertBatchSomeColumn对象 ，理解：添加了一个自定义的批量添加方法
     *  3. p->p.getFieldFill()!= FieldFill.UPDATE 是自定义的内容
     *  4. @Bean注解提交到Spring容器
     */
    @Bean
    public DefaultSqlInjector defaultSqlInjector(){

        log.debug("自动装配批量查询" );
        return  new DefaultSqlInjector(){
            @Override
            public List<AbstractMethod> getMethodList(Class<?> mapperClass, TableInfo tableInfo) {
                //获得父类的
                List<AbstractMethod> methodList = super.getMethodList(mapperClass, tableInfo);
                methodList.add(new InsertBatchSomeColumn(p->p.getFieldFill()!= FieldFill.UPDATE));
                return methodList;
            }
        };
    }
}