package com.wonu.settingsun.domain.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName Role
 * @date 2023/11/2 11:05
 * @description
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_role")
public class Role {
    @TableId("role_id")
    private Long roleId;
    @TableField("user_id")
    private Long userId;
    @TableField("role_name")
    private String roleName;
    @TableField("description")
    private String description;
    @TableField("create_time")
    private Date createTime;
    @TableField("update_time")
    private Date updateTime;
    @TableField("create_by")
    private String createBy;

}