package com.wonu.settingsun.domain.vo;

import java.util.Date;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * (VAppointmenorder)VO类
 *
 * @author makejava
 * @since 2023-11-10 10:15:43
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("v_appointment_order")
public class AppointmentOrderVO {
    /**
     * 预约订单id
     */
    @TableId(value = "appointment_id")
    private Integer appointmentId;
    /**
     * 医生账号
     */
    @TableField("doctor_name")
    private String doctorName;
    /**
     * 姓名
     */
    @TableField("doctor")
    private String doctor;
    /**
     * 病人账号
     */
    @TableField("patient_name")
    private String patientName;

    @TableField("patient")
    private String patient;
    /**
     * 预约时间
     */
    @TableField("appointment_time")
    private Date appointmentTime;
    /**
     * 0-取消；1-等待；2-完成
     */
    @TableField("order_status")
    private Integer orderStatus;
    /**
     * 预约描述
     */
    @TableField("content")
    private String content;

    @TableField("create_time")
    private String createTime;

}
