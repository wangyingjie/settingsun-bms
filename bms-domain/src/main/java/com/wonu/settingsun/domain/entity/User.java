package com.wonu.settingsun.domain.entity;

import java.math.BigDecimal;
import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (User)实体类
 *
 * @author makejava
 * @since 2023-11-08 17:28:45
 */
@Data
@TableName("t_user")
public class User {
    /**
     * 主键
     */
    @TableId(value = "user_id",type = IdType.AUTO)
    private Long userId;
    /**
     * 账号
     */    
    @TableField("username")
    private String username;
    /**
     * 密码
     */    
    @TableField("password")
    private String password;
    /**
     * 昵称
     */    
    @TableField("nikename")
    private String nikeName;
    /**
     * 余额
     */    
    @TableField("balance")
    private BigDecimal balance;
    /**
     * 角色id
     */    
    @TableField("role_id")
    private Long roleId;
    /**
     * 创建时间
     */    
    @TableField("create_time")
    private Date createTime;
    /**
     * 修改时间
     */    
    @TableField("update_time")
    private Date updateTime;
    /**
     * 创建人
     */    
    @TableField("create_by")
    private String createBy;

}
