package com.wonu.settingsun.domain.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ElderQueryDto
 * @date 2023/11/7 17:15
 * @description   健康信息查询实体类
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElderQueryDto {

    private String username;
    private String name;
    private Integer gender;
}