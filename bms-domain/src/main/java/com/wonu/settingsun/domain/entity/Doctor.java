package com.wonu.settingsun.domain.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (Doctor)实体类
 *
 * @author makejava
 * @since 2023-11-02 19:57:32
 */
@Data
@TableName("t_doctor")
public class Doctor {
    
    @TableId(value = "doctor_id")
    private Long doctorId;
    /**
     * 用户id
     */    
    @TableField("user_id")
    private Long userId;
        
    @TableField("name")
    private String name;
    /**
     * 1-男，0-女
     */    
    @TableField("gender")
    private Integer gender;
    /**
     * 联系电话
     */    
    @TableField("phone")
    private String phone;
    /**
     * 工作年限
     */    
    @TableField("experience")
    private Integer experience;
    /**
     * 擅长领域介绍
     */    
    @TableField("expertise_areas")
    private String expertiseAreas;
    /**
     * 资格证书号
     */    
    @TableField("license_number")
    private String licenseNumber;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
