package com.wonu.settingsun.domain.entity;

import java.util.Date;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
/**
 * (DoctorSchedule)实体类
 *
 * @author makejava
 * @since 2023-11-09 15:24:48
 */
@Data
@TableName("t_doctor_schedule")
public class DoctorSchedule {
    /**
     * 排班id
     */
    @TableId(value = "schedule_id")
    private Long scheduleId;
    /**
     * 医师账户名
     */    
    @TableField("doctor_name")
    private String doctorName;
    /**
     * 工作星期
     */    
    @TableField("day_of_week")
    private Integer dayOfWeek;
    /**
     * 工作量
     */    
    @TableField("workload")
    private Integer workload;
        
    @TableField("create_time")
    private Date createTime;
        
    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
