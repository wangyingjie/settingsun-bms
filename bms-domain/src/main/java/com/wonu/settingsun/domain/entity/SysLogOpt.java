package com.wonu.settingsun.domain.entity;

import java.util.Date;

import com.alibaba.excel.annotation.ExcelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.NoArgsConstructor;

/**
 * 日志表(SysLogOpt)实体类
 *
 * @author makejava
 * @since 2023-11-08 21:18:50
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_sys_log_opt")
public class SysLogOpt {
    /**
     * 主键ID
     */
    @ExcelProperty("日志编号")
    @TableId(value = "log_id")
    private Long id;
    /**
     * 请求方式
     */
    @ExcelProperty("请求方式")
    @TableField("req_type")
    private String reqType;
    /**
     * 请求url
     */
    @ExcelProperty("请求URL")
    @TableField("req_url")
    private String reqUrl;
    /**
     * 请求uri
     */
    @ExcelProperty("请求URI")
    @TableField("req_uri")
    private String reqUri;

    //操作人
    @ExcelProperty("操作人员")
    @TableField("operator")
    private String operator;
    /**
     * 操作方法
     */
    @ExcelProperty("执行方法")
    @TableField("method")
    private String method;
    /**
     * 注解描述
     */
    @ExcelProperty("方法描述")
    @TableField("opt_desc")
    private String optDesc;
    /**
     * 请求参数
     */
    @ExcelProperty("请求参数")
    @TableField("req_params")
    private String reqParams;
    /**
     * 返回结果
     */
    @ExcelProperty("返回结果")
    @TableField("resp_data")
    private String respData;
    /**
     * 操作时间
     */
    @ExcelProperty("执行时间")
    @TableField("opt_time")
    private Date optTime;
    /**
     * 请求耗时(ms)
     */
    @ExcelProperty("请求耗时")
    @TableField("spend_time")
    private Long spendTime;

}
