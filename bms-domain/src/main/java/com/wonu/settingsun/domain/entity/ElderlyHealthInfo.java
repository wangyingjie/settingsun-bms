package com.wonu.settingsun.domain.entity;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.*;

/**
 * (ElderlyHealthInfo)实体类
 *
 * @author makejava
 * @since 2023-11-06 19:13:36
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("t_elderly_health_info")
public class ElderlyHealthInfo {
    
    @TableId(value = "elder_id")
    private Integer elderId;
    /**
     * 关联账户
     */    
    @TableField("username")
    @NotBlank(message = "用户账户不能为空")  //作用于String类型
    private String username;
        
    @TableField("name")
    @NotBlank(message = "用户名不能为空")
    private String name;
    /**
     * 1-男；0-女
     */    
    @TableField("gender")
    @NotNull(message = "性别不能为空")
    private Integer gender;

    @NotNull(message = "年龄不能为空")
    @TableField("age")
    private Integer age;
    /**
     * 身份证号
     */    
    @TableField("idcard")
    @NotBlank(message = "身份证号不能为空")
    private String idcard;
    /**
     * 单位：m
     */    
    @TableField("height")
    @Digits(integer = 1, fraction = 2,message = "身高整数部分最多为1位,小数部分最多为2位")
    @DecimalMax(value = "2.50",message = "身高超出最大值")
    @DecimalMin(value = "0.5",message = "身高超出最小值")
    private Double height;
    /**
     * 单位：kg
     */    
    @TableField("weight")
    @Digits(integer = 3,fraction = 2,message = "体重数据整数位数最多3位，小数位数最多2位")
    @DecimalMax(value = "150.50",message = "体重超出最大值")
    @DecimalMin(value = "10.50",message = "体重超出最小值")
    private Double weight;
    /**
     * （体重指数），计算公式为：BMI = 体重(kg) / 身高²(m)
        偏瘦	<= 18.4
        正常	18.5 ~ 23.9
        过重	24.0 ~ 27.9
        肥胖	>= 28.0
     */    
    @TableField("bmi")
    @Digits(integer = 2,fraction = 2,message = "bmi数据整数位数最多2位，小数位数最多2位")
    @DecimalMax(value = "40.50",message = "bmi超出最大值")
    @DecimalMin(value = "10.50",message = "bmi超出最小值")
    private Double bmi;
    /**
     * 收缩压，即血压的高值，单位为毫米汞柱(mmHg)
     */    
    @TableField("systolic_blood_pressure")
    @Range(min = 90, max = 300,message = "收缩压数值不合理")
    private Integer systolicBloodPressure;
    /**
     * 舒张压，即血压的低值，单位为毫米汞柱(mmHg)
     */    
    @TableField("diastolic_blood_pressure")
    @Range(min = 50,max = 110,message = "舒张压数值不合理")
    private Integer diastolicBloodPressure;
    /**
     * 心率，单位为次/分钟
     */    
    @TableField("heart_rate")
    @Range(min= 30, max = 240,message = "心率数值不合理")
    private Integer heartRate;
    /**
     * 空腹血糖值，单位为毫克/分升(mg/dL)
     */    
    @TableField("fasting_blood_glucose")
    @Range(min= 50, max = 200,message = "空腹血糖数值不合理")
    private Integer fastingBloodGlucose;
    /**
     * 总胆固醇值，单位为毫克/分升(mg/dL)
     */    
    @TableField("total_cholesterol")
    @Range(min= 100, max = 240,message = "总胆固醇数值不合理")
    private Integer totalCholesterol;
    /**
     * 测量日期
     */    
    @TableField("measurement_date")
    @PastOrPresent(message = "日期必须是今天或之前的日期")
    @JsonFormat(pattern = "yyyy-MM-dd")
    private Date measurementDate;
        
    @TableField("create_time")
//    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone = "GMT+8") 已经全局配置了
    private Date createTime;

    @TableField("update_time")
    private Date updateTime;
        
    @TableField("create_by")
    private String createBy;

}
