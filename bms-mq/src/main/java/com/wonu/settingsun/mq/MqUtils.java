package com.wonu.settingsun.mq;

import com.wonu.settingsun.mq.message.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MqUtils
 * @date 2023/11/9 21:51
 * @description
 */
@Component
public class MqUtils {

    @Resource
    private RabbitTemplate rabbitTemplate;


    /**
     * @author nameless
     * @date 2023/11/9 21:49
     * @param message
     * @return void
     * @description 向mq队列发送消息体，进行排序
     */
    public void routingProduce(String message, String exchange, String routingKey) {

        rabbitTemplate.convertAndSend(exchange, routingKey, message);

    }
}