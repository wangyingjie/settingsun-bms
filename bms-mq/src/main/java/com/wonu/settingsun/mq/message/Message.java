package com.wonu.settingsun.mq.message;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName Message
 * @date 2023/11/9 19:29
 * @description mq的消息体
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Message implements Serializable {

    private String methodName;
    private List<Object> args;
    private String desc;

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public List<Object> getArgs() {
        return args;
    }

    public void setArgs(Object... args) {
        this.args = new ArrayList<>();
        this.args.addAll(Arrays.asList(args));
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}