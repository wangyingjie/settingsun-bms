package com.wonu.settingsun.mq.config;

import com.wonu.settingsun.mq.MQKey;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RabbitMqConfig
 * @date 2023/11/9 14:29
 * @description   mq 的配置类
 */
@Configuration
@Slf4j
public class RabbitMqConfig {

    @Bean
    public MessageConverter messageConverter() {
        return new Jackson2JsonMessageConverter();
    }


    @Bean
    public RabbitTemplate rabbitTemplate(ConnectionFactory connectionFactory) {

        log.debug("自动装配json字符串的rabbitTemplate");
        // 调用模板类有参的构造
        RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        // 设置信息转化 使用的是jackson转化json的转化类 ,目的是实现mq中实体类自动转化为json字符串
        rabbitTemplate.setMessageConverter(messageConverter());

        return rabbitTemplate;
    }

    @Bean
    public DirectExchange directExchange() {
        return new DirectExchange("settingSun_module_exchange",true, false);
    }

    /**
     * @author nameless
     * @date 2023/11/9 17:22
     * @param
     * @return Queue
     * @description 用以给bms-doctor模块发送通知的队列
     */
    @Bean
    public Queue doctorQueue() {
        return new Queue("settingSun_doctor_queue");
    }

    @Bean
    public Queue orderQueue() {
        return new Queue("settingSun_order_queue");
    }



    /**
     * @author nameless
     * @date 2023/11/9 19:21
     * @param
     * @return Binding
     * @description routingKey是枚举字符串， 消费的队列是枚举的值
     */
    @Bean
    public Binding bindingRoutingWithDoctor() {
        return BindingBuilder.bind(doctorQueue())
                .to(directExchange()).with(MQKey.DOCTOR);
    }

    @Bean
    public Binding bindingRoutingWithOrder() {
        return BindingBuilder.bind(orderQueue())
                .to(directExchange()).with(MQKey.ORDER);
    }

}