package com.wonu.settingsun.mq;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MQKey
 * @date 2023/11/9 17:25
 * @description  mq的routingkey枚举
 */
public enum MQKey {

    //
    DOCTOR("settingSun_doctor_queue"),
    MODULE_EXCHANGE("settingSun_module_exchange"),
    ORDER("settingSun_order_queue"),
    ;

    String name;
    MQKey(String queueName) {
        this.name = queueName;
    }

    public String getName() {
        return name;
    }
}
