package com.wonu.settingsun.common.exception;


import com.wonu.settingsun.common.result.ErrorCode;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BmsException
 * @date 2023/11/2 14:40
 * @description  自定义异常基类
 */
@Data
@NoArgsConstructor
public class BmsException extends RuntimeException{
    private int code;
    private String message;


    public BmsException(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public BmsException(ErrorCode eCode) {
        this.code = eCode.getCode();
        this.message = eCode.getMessage();
    }
}