package com.wonu.settingsun.common.result;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author nameless
 * @version 1.0
 * @ClassName HttpResp
 * @date 2023/11/2 14:14
 * @description    统一返回结果类
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class HttpResp <T>{
    private int code;
    private String message;
    private T data;

    public static <T> HttpResp<T> success(int code, String message, T data) {
        return new HttpResp<T>(code, message, data);
    }
    public static <T> HttpResp<T> success(RespCode respCode, T data) {
        return new HttpResp<T>(respCode.getCode(), respCode.getMessage(), data);
    }

    public static <T> HttpResp<T> isSuccess(boolean flag, String operate) {
        if(flag) {
            return new HttpResp<T>(200, operate + "操作成功", null);
        }else{
            return new HttpResp<T>(400, operate + "操作失败",null);
        }

    }

    public static <T> HttpResp<T> fail(int code, String message, T data) {
        return new HttpResp<T>(code, message, data);
    }
    public static <T> HttpResp<T> fail(int code, String message) {
        return new HttpResp<T>(code, message, null);
    }
    public static <T> HttpResp<T> fail(RespCode respCode, T data) {
        return new HttpResp<T>(respCode.getCode(), respCode.getMessage(), data);
    }




}