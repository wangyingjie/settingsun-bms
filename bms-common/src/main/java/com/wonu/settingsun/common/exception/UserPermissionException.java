package com.wonu.settingsun.common.exception;


import com.wonu.settingsun.common.result.ErrorCode;

/**
 * ClassName： UserPermissionException
 *
 * @author nameless
 * Description：    用户权限异常
 * @date 2023/10/20 17:18
 */
public class UserPermissionException extends BmsException{
    public UserPermissionException(int code, String message) {
        super(code, message);
    }

    public UserPermissionException(ErrorCode eCode) {
        super(eCode);
    }
}