package com.wonu.settingsun.common.exception;

import com.wonu.settingsun.common.result.HttpResp;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.util.List;
import java.util.Set;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ExceptionHandler
 * @date 2023/11/2 14:32
 * @description  全局异常处理类【应当自动装配】
 */
@RestControllerAdvice
@Slf4j
public class GlobalHandler {

    {
        log.debug("自动装配全局异常处理类");
    }

    @ExceptionHandler(BmsException.class)
    public HttpResp bmsException(BmsException e) {
        return HttpResp.fail(e.getCode(), e.getMessage());
    }

    @ExceptionHandler(AuthenticationException.class)
    public HttpResp authenticationException(AuthenticationException e) {
        return HttpResp.fail(e.getCode(), e.getMessage());
    }
    @ExceptionHandler(UserPermissionException.class)
    public HttpResp userPermissionException(UserPermissionException e) {
        return HttpResp.fail(e.getCode(),e.getMessage());
    }
    @ExceptionHandler(InputParameterException.class)
    public HttpResp inputParameterException(InputParameterException e) {
        return HttpResp.fail(e.getCode(), e.getMessage());
    }
    @ExceptionHandler(EncryptException.class)
    public HttpResp encryptException(EncryptException e) {
        return HttpResp.fail(e.getCode(), e.getMessage());
    }
    @ExceptionHandler(RegexException.class)
    public HttpResp regexException(RegexException e) {
        return HttpResp.fail(e.getCode(), e.getMessage());
    }

    /**
     * 用于捕获@RequestBody类型参数触发校验规则抛出的异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public HttpResp handleValidException(MethodArgumentNotValidException e) {
        StringBuilder sb = new StringBuilder();
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        if (!CollectionUtils.isEmpty(allErrors)) {
            for (ObjectError error : allErrors) {
                sb.append(error.getDefaultMessage()).append(";");
            }
        }
        String message = sb.toString();
        return HttpResp.fail(503,message);
    }

    /**
     * 用于捕获@RequestParam/@PathVariable参数触发校验规则抛出的异常
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public HttpResp handleConstraintViolationException(ConstraintViolationException e) {
        StringBuilder sb = new StringBuilder();
        Set<ConstraintViolation<?>> conSet = e.getConstraintViolations();
        for (ConstraintViolation<?> con : conSet) {
            String message = con.getMessage();
            sb.append(message).append(";");
        }
        return HttpResp.fail(503, sb.toString());
    }

}