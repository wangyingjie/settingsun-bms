package com.wonu.settingsun.common.exception;



import com.wonu.settingsun.common.result.ErrorCode;

/**
 * ClassName： AuthenticationException
 *
 * @author nameless
 * Description： token认证异常
 * @date 2023/10/26 14:43
 */

public class AuthenticationException extends BmsException {

    public AuthenticationException(int code, String message) {
        super(code, message);
    }

    public AuthenticationException(ErrorCode eCode) {
        super(eCode);
    }
}