package com.wonu.settingsun.common.utils;


/**
 * @author nameless
 * @version 1.0
 * @ClassName ValidationUtils
 * @date 2023/11/4 10:03
 * @description  输入校验工具类
 */
public class StringValidateUtils {

    //3-15位 字母 数字 下划线
    public static final String NAME_REGEX = "^[a-zA-Z0-9_]{3,15}$";
    //必须包含至少一个大写字母、一个小写字母和一个数字  允许使用特殊字符  长度为 6-20 个字符
    public static final String PASSWORD_REGEX = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d\\S]{6,20}$";
    public static final String PHONE_REGEX = "^1[3-9]\\d{9}$";
    public static final String IDCARD_REGEX
            = "^[1-9]\\d{5}(19|20)\\d{2}((0[1-9])|(1[0-2]))(([0-2][1-9])|10|20|30|31)\\d{3}[Xx\\d]$";


    /**
     * @author nameless
     * @date 2023/11/4 10:10
     * @param input
     * @return boolean
     * @description 判断字符串是否有内容
     */
    public static boolean hasText(String input) {
        return input != null && !input.isEmpty() && containsText(input);
    }


    public static boolean regexCheck(String input, String regex) {
        return input.matches(regex);
    }

    public static boolean usernameRegexCheck(String input) {
        return input.matches(NAME_REGEX);
    }
    public static boolean passwordRegexCheck(String input) {
        return input.matches(PASSWORD_REGEX);
    }
    public static boolean phoneRegexCheck(String input) {
        return input.matches(PHONE_REGEX);
    }public static boolean idCardRegexCheck(String input) {
        return input.matches(IDCARD_REGEX);
    }


    /**
     * @author nameless
     * @date 2023/11/4 10:09
     * @param str
     * @return boolean
     * @description StringUtils 判断字符串是否含有内容的源码
     */
    private static boolean containsText(CharSequence str) {
        int strLen = str.length();

        for(int i = 0; i < strLen; ++i) {
            if (!Character.isWhitespace(str.charAt(i))) {
                return true;
            }
        }

        return false;
    }

}