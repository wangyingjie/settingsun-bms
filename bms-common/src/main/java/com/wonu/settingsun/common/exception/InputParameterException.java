package com.wonu.settingsun.common.exception;


import com.wonu.settingsun.common.result.ErrorCode;

/**
 * @author nameless
 * @version 1.0
 * @ClassName InputParameterException
 * @date 2023/10/30 20:11
 * @description   参数输入异常
 */

public class InputParameterException extends BmsException {


    public InputParameterException(int code, String message) {
        super(code, message);
    }

    public InputParameterException(ErrorCode eCode) {
        super(eCode);
    }
}