package com.wonu.settingsun.common.result;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RespCode
 * @date 2023/11/2 14:14
 * @description   服务器响应码枚举
 */
public enum RespCode {

    OK(200, "请求成功");

    private int code;
    private String message;

    RespCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}