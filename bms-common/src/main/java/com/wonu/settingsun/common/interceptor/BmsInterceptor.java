package com.wonu.settingsun.common.interceptor;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTDecodeException;
import com.auth0.jwt.exceptions.SignatureVerificationException;
import com.auth0.jwt.exceptions.TokenExpiredException;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.wonu.settingsun.common.annotation.Role;
import com.wonu.settingsun.common.exception.AuthenticationException;
import com.wonu.settingsun.common.exception.UserPermissionException;
import com.wonu.settingsun.common.result.ErrorCode;
import com.wonu.settingsun.common.utils.EncryptUtils;
import com.wonu.settingsun.common.utils.RedisUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BmsInterceptor
 * @date 2023/11/2 16:01
 * @description  系统拦截器：主要进行请求的token验证  普通类可以被spring进行依赖注入，但最好都交给spring进行管理
 */
@Slf4j
public class BmsInterceptor implements HandlerInterceptor {

    @Resource
    private RedisUtils redisUtils;
    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {

        log.debug("进入拦截器");

        String jwt = request.getHeader("token");
        if(!StringUtils.hasText(jwt)) throw new AuthenticationException(ErrorCode.TOKENISEMPTY);

        String saltKey =(String)redisUtils.get(jwt);
        if(Objects.isNull(saltKey)) throw new AuthenticationException(ErrorCode.TOKENNOTFOUND);

        String salt = EncryptUtils.decrypt(saltKey);
        JWTVerifier verifier = JWT.require(Algorithm.HMAC256(salt)).build();
        try {
            verifier.verify(jwt);
        }catch (TokenExpiredException exception) {
            //若token过期则自动续期，先判断以下redis中是否还有时效[再重复一次是为了避免执行到这里，redis刚好过期]
            //过期了也可以解析，用来创建新token
            Long expireTime = redisTemplate.getExpire(jwt, TimeUnit.MICROSECONDS);
            if( expireTime> 0) {
                String newToken = reFreshToken(jwt, salt,expireTime);
                Cookie[] cookies = request.getCookies();
                for (Cookie c : cookies) {
                    if ("jwt".equals(c.getName())) {
                        c.setValue(newToken);
                    }
                }
            }else{
                throw new AuthenticationException(ErrorCode.TOKENTIMEOUT);
            }
        }catch (SignatureVerificationException | JWTDecodeException e) {
            // 篡改异常
            throw new AuthenticationException(ErrorCode.TOKENDECODEERROR);
        }

        //若是鉴权，则是从JWt中获取roleId，获取请求的路径，解析路径上的@Role注解中内容，有角色id则可以放行
        //token验证完成正确   解析jwt中payload需要使用存入时的value
        DecodedJWT decodedJWT = JWT.decode(jwt);
        Long roleId = decodedJWT.getClaim("roleId").asLong();
        String username = decodedJWT.getClaim("nikeName").asString();
        log.debug("roleId:{}",roleId);

        //handler 是当前controller请求访问的方法   : com.wonu.settingsun.user.controller.UserController#findAll()
        HandlerMethod method = (HandlerMethod) handler;
        Role bmsRole= method.getMethod().getDeclaredAnnotation(Role.class);
        if(Objects.nonNull(bmsRole)) {
            String[] roleIds = bmsRole.values().split(",");
            List<Long> requiredRolIds = Arrays.stream(roleIds)
                    .map(Long::parseLong).collect(Collectors.toList());

            if(!requiredRolIds.contains(roleId)) {
                throw new UserPermissionException(ErrorCode.USERPERMISSIONERROR);
            }
        }

        return true;
    }

    /**
     * @author nameless
     * @date 2023/10/26 20:26
     * @param oldJwt 过期jwt
     * @param salt  redis中的盐
     * @return String  新的jwt
     * @description  刷新JWT,续期的token存活时间为redis剩余时间
     */
    private String reFreshToken(String oldJwt, String salt, long expireTime) {

        Map<String, Claim> claims = JWT.decode(oldJwt).getClaims();
        Map<String, String> claimsMap = claims.entrySet().stream().
                collect(Collectors.toMap(Map.Entry::getKey, entry -> entry.getValue().toString()));
        //从旧token中截取payload
        String newJwt = JWT.create().withPayload(claimsMap)
                .withExpiresAt(new Date(System.currentTimeMillis() + expireTime))
                .sign(Algorithm.HMAC256(salt));
       /* String newJwt = JWT.create().withClaim("username", username)
                .withExpiresAt(deadLine)
                .sign(Algorithm.HMAC256(salt));*/
        //是否可以重置是存在时间？
        //redisTemplate.expire(oldJwt, 1000 * 60 * 5 * 2, TimeUnit.MINUTES);
        return newJwt;
    }
}