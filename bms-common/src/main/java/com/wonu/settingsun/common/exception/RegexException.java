package com.wonu.settingsun.common.exception;

import com.wonu.settingsun.common.result.ErrorCode;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RegexException
 * @date 2023/10/30 20:22
 * @description  正则校验异常
 */

public class RegexException extends BmsException {


    public RegexException(int code, String message) {
        super(code, message);
    }

    public RegexException(ErrorCode eCode) {
        super(eCode);
    }
}