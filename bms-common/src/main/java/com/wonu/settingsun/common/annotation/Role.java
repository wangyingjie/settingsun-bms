package com.wonu.settingsun.common.annotation;

import java.lang.annotation.*;
import java.util.List;
import java.util.Set;

/**
 * @author nameless
 * @version 1.0
 * @ClassName Role
 * @date 2023/11/3 16:54
 * @description  自定义角色注解 可以加到类或方法上
 */
@Target({ElementType.METHOD,ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Role {

     String values() ;
}
