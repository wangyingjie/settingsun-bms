package com.wonu.settingsun.common.exception;

import com.wonu.settingsun.common.result.ErrorCode;

/**
 * @author nameless
 * @version 1.0
 * @ClassName EncryptException
 * @date 2023/10/28 18:35
 * @description   加密异常类
 */
public class EncryptException extends BmsException {

    public EncryptException(int code, String message) {
        super(code, message);
    }

    public EncryptException(ErrorCode eCode) {
        super(eCode);
    }
}