package com.wonu.settingsun.common.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BloomUtils
 * @date 2023/11/3 11:45
 * @description  布隆过滤器管理类  布隆过滤器为String-String类型
 */
@Component
public class BloomUtils {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * @author nameless
     * @date 2023/11/3 12:58
     * @param bloomKey 布隆过滤器的key
     * @param arg   元素值（会以String格式存储）
     * @return boolean
     * @description 添加元素至布隆过滤器
     */
    public boolean addToBloom(String bloomKey, Object arg) {
        Long execute = redisTemplate.opsForValue().getOperations().execute(
                new DefaultRedisScript<>("return redis.call('bf.add', KEYS[1], ARGV[1])", Long.class)
                , new ArrayList<String>() {{
                    add(bloomKey);
                }}
                , arg
        );
        return execute == 1;
    }
    /**
     * @author nameless
     * @date 2023/11/3 12:59
     * @param bloomKey
     * @return boolean
     * @description 删除整个布隆过滤器
     */
    public boolean deleteBloom(String bloomKey) {
        Long execute = redisTemplate.opsForValue().getOperations().execute(
                new DefaultRedisScript<>("return redis.call('del', KEYS[1])", Long.class)
                , new ArrayList<String>() {{
                    add(bloomKey);
                }}
        );
        return execute == 1;
    }

    /**
     * @author nameless
     * @date 2023/11/3 12:59
     * @param bloomKey  原有的布隆key
     * @param values  布隆内的元素集合
     * @return void
     * @description 重建刷新布隆过滤器
     */
    public void refreshBloom(String bloomKey, List<String> values) {

        deleteBloom(bloomKey);
        values.forEach(val-> addToBloom(bloomKey,val));
    }
}