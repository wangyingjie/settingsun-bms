package com.wonu.settingsun.common.utils;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RedisUtils
 * @date 2023/11/2 17:48
 * @description  redis封装工具类
 */
@Component
public class RedisUtils {

    @Resource
    private  RedisTemplate<String, Object> redisTemplate;

    public String get(String key) {
        return (String) redisTemplate.opsForValue().get(key);
    }

    public void set(String key, String value) {
        redisTemplate.opsForValue().set(key, value);
    }

    /**
     * @author nameless
     * @date 2023/11/2 17:57
     * @param key    键
     * @param value  值
     * @param time  秒数
     * @return void
     * @description 设置key-value、和ttl时间，单位秒  MILLISECONDS是毫秒!
     */
    public  void set(String key, String value, long time) {
        if(time > 0) {
            redisTemplate.opsForValue().set(key, value, time,TimeUnit.MILLISECONDS);
        }
        else {
            redisTemplate.opsForValue().set(key, value);
        }
    }

    /**
     * @author nameless
     * @date 2023/11/2 18:51
     * @param key   hash的key
     * @param hkey  hash中成员的key
     * @return Object  成员的值
     * @description   获取hash中某项item的值
     */
    public Object hget(String key, String hkey) {
        return redisTemplate.opsForHash().get(key, hkey);
    }

    /**
     * @author nameless
     * @date 2023/11/2 19:04
     * @param key   hash key
     * @return Map<Object,Object>
     * @description 获取hash的map集合
     */
    public Map<Object, Object> hEntries(String key) {
        return redisTemplate.opsForHash().entries(key);
    }

    public void hset(String key, String hkey, Object value) {
         redisTemplate.opsForHash().put(key, hkey, value);
    }

    public void hset(String key, String hkey, Object value, long time) {
        redisTemplate.opsForHash().put(key, hkey, value);
        if(time > 0) {
            redisTemplate.expire(key,time,TimeUnit.MILLISECONDS);
        }
    }

    /**
     * @author nameless
     * @date 2023/11/2 18:58
     * @param key  hash 的key
     * @param map   map 为 key-value的集合
     * @return void
     * @description 批量k-v到hash中
     */
    public void hmset(String key, Map<String, Object> map) {
        redisTemplate.opsForHash().putAll(key,map);
    }

    public void hmset(String key, Map<String, Object> map, long time) {
        redisTemplate.opsForHash().putAll(key,map);
        if(time > 0) {
            redisTemplate.expire(key,time,TimeUnit.MILLISECONDS);
        }
    }

    public void deleteHashKey(String key, String hashKey) {
        redisTemplate.opsForHash().delete(key, hashKey);
    }


    public void deleteKey(String key) {
        redisTemplate.delete(key);
    }


    public void descHValue(String key, String Hkey) {

        redisTemplate.opsForHash().increment(key, Hkey, -1);
    }

    public void incrHValue(String key, String Hkey) {

        redisTemplate.opsForHash().increment(key, Hkey, 1);
    }
}