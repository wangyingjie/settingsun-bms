package com.wonu.settingsun.common.result;

/**
 * @author nameless
 * @version 1.0
 * @ClassName Const
 * @date 2023/11/3 15:45
 * @description   项目常量枚举
 */
public class Const {
    public static final long DAY_TIME = 24 * 60 * 60 * 1000;
    public static final long MINUTES_TIME = 1000 * 60 * 10;
    public static final long MINUTE_TIME = 1000 * 60 * 1;
    public static final long HOURS_TIME = 1000 * 60 * 60 * 6;
    
    public static final String DES_SALT = "23452345";

    //权限区域
    public static final String COMMON_AREA ="1,2,3,4";
    public static final String ADMIN = "1";
    public static final String TEST_AREA = "2,3,4";

    public static final String SCHEDULETIME = "00 02 10 * * ?";

    public static final String LOGSCHEDULETIME = "00 00 00 * * ?";
}
