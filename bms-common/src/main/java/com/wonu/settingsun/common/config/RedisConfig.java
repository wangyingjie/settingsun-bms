package com.wonu.settingsun.common.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.data.redis.cache.RedisCacheConfiguration;
import org.springframework.data.redis.cache.RedisCacheManager;
import org.springframework.data.redis.cache.RedisCacheWriter;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.serializer.GenericJackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.RedisSerializationContext;
import org.springframework.data.redis.serializer.RedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.time.Duration;
import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RedisConfig
 * @date 2023/11/2 15:24
 * @description   redis配置类，配置redisTemplate （单独使用） 和 redis 管理器（结合Spring缓存注解使用）
 */
@Configuration
@Slf4j
public class RedisConfig {


    /**
     * @author nameless
     * @date 2023/11/2 15:40
     * @param redisConnectionFactory
     * @return RedisTemplate<String,Object>
     * @description 自定义的RedisTemplate模板
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory redisConnectionFactory) {

        // 创建一个 RedisTemplate
        RedisTemplate<String, Object> redisTemplate =
                new RedisTemplate<>();
        //调用setConnectionFactory（继承自RedisAccess类）将redisConnectionFactory放入模板
        redisTemplate.setConnectionFactory(redisConnectionFactory);
        // 通用的 key- value 的序列化
        redisTemplate.setKeySerializer(RedisSerializer.string());
        redisTemplate.setValueSerializer(RedisSerializer.string());
        // hash类型 hashkey hashvalue 序列化
        redisTemplate.setHashKeySerializer(RedisSerializer.string());
        redisTemplate.setHashValueSerializer(RedisSerializer.json());
        log.debug("自动装配自定义序列化的redisTemplate");
        return redisTemplate;
    }


    @Bean
    public RedisCacheManager redisCacheManager(RedisConnectionFactory factory) {
        //创建一个redisCacheWriter
        RedisCacheWriter redisCacheWriter = RedisCacheWriter.nonLockingRedisCacheWriter(factory);
        //进行redis管理器的配置，Key、value序列化
        RedisCacheConfiguration redisCacheConfiguration = RedisCacheConfiguration.defaultCacheConfig()
                .serializeKeysWith(RedisSerializationContext.SerializationPair.fromSerializer(RedisSerializer.string()))
                .serializeValuesWith(RedisSerializationContext.SerializationPair.fromSerializer(RedisSerializer.json()))
                .entryTtl(Duration.ofMinutes(30))
                .disableCachingNullValues();
        log.debug("自动装配RedisCacheManager");
        return new RedisCacheManager(redisCacheWriter, redisCacheConfiguration);
    }

    /**
     * @author nameless
     * @date 2023/10/24 17:30
     * @param
     * @return null
     * @description 配置DefaultRedisScript 类，是脚本类 ,加载类路径下的lua脚本
     */
      /*  @Bean
        public DefaultRedisScript<String> defaultRedisScript() {
            DefaultRedisScript<String> defaultRedisScript = new DefaultRedisScript<>();
            *//*设置返回值*//*
            defaultRedisScript.setResultType(String.class);
            *//*ClassPathResource 资源加载*//*
            defaultRedisScript.setLocation(new ClassPathResource("lua/hello.lua"));
            return defaultRedisScript;
        }*/
}