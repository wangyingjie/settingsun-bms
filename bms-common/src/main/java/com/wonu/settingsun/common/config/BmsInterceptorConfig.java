package com.wonu.settingsun.common.config;

import com.wonu.settingsun.common.interceptor.BmsInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author nameless
 * @version 1.0
 * @ClassName BmsInterceptorConfig
 * @date 2023/11/2 16:07
 * @description
 */
@Configuration
@Slf4j
public class BmsInterceptorConfig implements WebMvcConfigurer {


    @Bean
    public BmsInterceptor bmsInterceptor() {
        log.debug("自动装配Spring拦截器");
        return new BmsInterceptor();
    }

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        /*registry.addInterceptor(bmsInterceptor())
                .addPathPatterns("/api/**")
                .excludePathPatterns("/api/user/login"
                ,"/api/user/register"
                ,"/api/img/code"
                ,"/api/img/flush");*/
    }
}