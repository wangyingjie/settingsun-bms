package com.wonu.settingsun.common.utils;

import java.time.ZoneId;
import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DateUtils
 * @date 2023/11/9 20:11
 * @description  时间处理类
 */
public class DateUtils {

    public static int dayOfWeek(Date date) {

        return (date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate().getDayOfWeek().getValue()) % 7;
    }
}