package com.wonu.settingsun.common.result;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ExceptionCode
 * @date 2023/11/2 14:25
 * @description  自定义的业务异常码
 */
public enum ErrorCode {
    USERPERMISSIONERROR(401, "用户权限非法"),
    USERNAMEISENPTY(402, "用户名为空"),
    USERNAMENOTFOUND(403,"用户不存在"),
    PASSWORDISENPTY(404, "密码为空"),
    ROLEISENPTY(405,"用户角色为空"),
    USERNAMEORPASSWORDERROR(501,"用户名或密码错误"),
    INPUTEMPTY(502,"输入参数为空"),
    REGEXERROR(503, "输入不符合格式" ),
    USERNAMEREGEX(503, "用户名必须以3-15位字母、数字、下划线组成"),
    PASSWOERDREGEX(503, "密码包含至少一个大写字母、一个小写字母和一个数字,允许使用特殊字符,长度为 6-20 个字符"),
    DUPLICATE(504, "用户名已存在"),
    CAPTCHACODEERROR(505,"验证码错误" ),
    CAPTCHACODETIMEOUT(505,"验证码已失效，请刷新后重新输入"),
    TOKENISEMPTY(600,"当前请求未携带token"),
    TOKENDECODEERROR(603,"jwt签名异常"),
    TOKENTIMEOUT(602,"jwt已过期"),
    TOKENNOTFOUND(601,"当前token在缓存中不存在"),
    ENCRYPTERROR(701,"加密失败"),
    DECRYPTERROR(702,"解密失败"),
    BLOOMERROR(800, "布隆过滤器失效，用户不存在") ;

    private int code;
    private String message;

    ErrorCode(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
