package com.wonu.settingsun.doctor.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.Doctor;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DoctorDao
 * @date 2023/11/9 15:21
 * @description
 */
@Mapper
public interface DoctorDao extends BaseMapper<Doctor> {
}
