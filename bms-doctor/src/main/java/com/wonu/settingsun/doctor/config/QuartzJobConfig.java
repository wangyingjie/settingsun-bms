package com.wonu.settingsun.doctor.config;

import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.doctor.scheduleJob.ScheduleQuartzJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author nameless
 * @version 1.0
 * @ClassName QuartzJobConfig
 * @date 2023/11/9 20:31
 * @description
 */
@Configuration
public class QuartzJobConfig {

    //创建任务细节
    @Bean
    public JobDetail jobDetails() {
        return JobBuilder.newJob(ScheduleQuartzJob.class)
                .storeDurably(true)  //使持久存在
                .build();
    }

    //创建触发器
    @Bean
    public Trigger doctrigger() {
        return TriggerBuilder.newTrigger().forJob(jobDetails())//绑定任务细节
                .withSchedule(CronScheduleBuilder.cronSchedule(Const.SCHEDULETIME))
                .build();
    }
}