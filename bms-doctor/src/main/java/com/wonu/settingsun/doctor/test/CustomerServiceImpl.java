package com.wonu.settingsun.doctor.test;

import com.wonu.settingsun.mq.MQKey;
import com.wonu.settingsun.mq.message.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Service;

/**
 * @author nameless
 * @version 1.0
 * @ClassName CustomerServiceImpl
 * @date 2023/11/9 14:54
 * @description
 */
@Service
public class CustomerServiceImpl implements CustomerService {

//    @RabbitListener(queues = "settingSun_doctor_queue")
    @Override
    public void customer(Message message) {
        System.out.println("[doctor]获取消息：" + message);
        String methodName = message.getMethodName();
    }
}