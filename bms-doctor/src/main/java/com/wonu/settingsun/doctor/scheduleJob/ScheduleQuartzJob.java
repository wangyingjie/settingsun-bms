package com.wonu.settingsun.doctor.scheduleJob;

import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.utils.DateUtils;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.doctor.service.ScheduleService;
import com.wonu.settingsun.domain.entity.DoctorSchedule;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ScheduleQuartzJob
 * @date 2023/11/9 20:32
 * @description   定时任务，获取医师排班表
 */

@Slf4j
public class ScheduleQuartzJob extends QuartzJobBean {

    @Resource
    private ScheduleService scheduleService;

    @Resource
    private RedisUtils redisUtils;

    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {

        log.debug("预加载医师排班");


       /* Message message = new Message();
        message.setArgs(today);
        message.setDesc("测试");
        message.setMethodName("findAllByDate");
        rabbitTemplate.convertAndSend(MQKey.MODULE_EXCHANGE.getName(), MQKey.DOCTOR.toString(), message);
*/
        scheduleService.warmUpSchedule();

    }

}