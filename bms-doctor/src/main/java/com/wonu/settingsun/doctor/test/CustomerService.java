package com.wonu.settingsun.doctor.test;

import com.wonu.settingsun.mq.message.Message;

/**
 * @author nameless
 * @version 1.0
 * @ClassName CustomerService
 * @date 2023/11/9 14:54
 * @description
 */
public interface CustomerService {

    void customer(Message message);

}
