package com.wonu.settingsun.doctor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author nameless
 * @version 1.0
 * @ClassName doctorApp
 * @date 2023/11/9 14:37
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wonu.settingsun"})
public class DoctorApp {
    public static void main(String[] args) {
        SpringApplication.run(DoctorApp.class,args);
    }
}