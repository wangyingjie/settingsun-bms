package com.wonu.settingsun.doctor.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.DoctorSchedule;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ScheduleDao
 * @date 2023/11/9 16:21
 * @description
 */
@Mapper
public interface ScheduleDao extends BaseMapper<DoctorSchedule> {
}
