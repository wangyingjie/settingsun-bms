package com.wonu.settingsun.doctor.test;

import com.wonu.settingsun.mq.MQKey;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MqServiceImpl
 * @date 2023/11/9 14:53
 * @description
 */
@Service
public class MqServiceImpl implements MqService {

    @Resource
    private RabbitTemplate rabbitTemplate;
    @Override
    public void produce(String str) {
        rabbitTemplate.convertAndSend(MQKey.MODULE_EXCHANGE.getName(), MQKey.DOCTOR.toString(), str);
    }
}