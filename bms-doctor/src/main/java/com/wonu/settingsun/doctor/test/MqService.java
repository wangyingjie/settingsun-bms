package com.wonu.settingsun.doctor.test;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MqSerivce
 * @date 2023/11/9 14:53
 * @description
 */
public interface MqService {

    void produce(String str);
}
