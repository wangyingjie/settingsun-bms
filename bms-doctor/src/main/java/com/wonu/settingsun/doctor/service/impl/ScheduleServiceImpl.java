package com.wonu.settingsun.doctor.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wonu.settingsun.common.exception.InputParameterException;
import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.result.ErrorCode;
import com.wonu.settingsun.common.utils.DateUtils;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.doctor.dao.ScheduleDao;
import com.wonu.settingsun.doctor.service.ScheduleService;
import com.wonu.settingsun.domain.entity.DoctorSchedule;
import io.swagger.models.auth.In;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ScheduleServiceImpl
 * @date 2023/11/9 16:21
 * @description
 */
@Service
public class ScheduleServiceImpl implements ScheduleService {

    @Resource
    private ScheduleDao scheduleDao;

    @Resource
    private RedisUtils redisUtils;

    /**
     * @author nameless
     * @date 2023/11/9 20:54
     * @param date
     * @return List<DoctorSchedule>
     * @description 根据日期获取未来两天的排班记录
     */
    @Override
    public List<DoctorSchedule> findAllByDate(Date date) {

        LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        //1-星期一 7-星期天
        int weekNumber = localDate.getDayOfWeek().getValue();
        int day = (weekNumber + 1) % 7;
        int dayAn = (weekNumber + 2) % 7;

        return scheduleDao.selectList(new QueryWrapper<DoctorSchedule>().in("day_of_week", day,dayAn));

    }

    /**
     * @author nameless
     * @date 2023/11/9 20:55
     * @param date
     * @return Map<String,Integer>
     * @description 根据预约日期获取redis中的key-value， 日期要进行校验（或者redis中就没有）
     */
    @Override
    public Map<String, Integer> findSchedulesInRedisByDate(Date date) {

        String day = String.valueOf(DateUtils.dayOfWeek(date));
        Map<Object, Object> hEntries = redisUtils.hEntries(day);
        if(hEntries.size() == 0) throw new InputParameterException(500, "预约日期异常");

        Map<String,Integer> doctors = new HashMap<>();
        for (Map.Entry<Object, Object> entry : hEntries.entrySet()) {
            String name = (String) entry.getKey();
            Integer workLoad = (Integer) entry.getValue();
            if(workLoad > 0) {
                doctors.put(name, workLoad);
            }
        }
        return doctors;
    }

    public void warmUpSchedule() {
        Date today = new Date();
        int day = DateUtils.dayOfWeek(today);
        String tomorrow = String.valueOf((day + 1)%7);
        String afterTomorrow = String.valueOf((day + 2)%7);

        List<DoctorSchedule> schedules = findAllByDate(today);
        //并行流？
        List<DoctorSchedule> tSchedules = schedules.stream().filter(sch -> sch.getDayOfWeek() == (day + 1) % 7).collect(Collectors.toList());
        List<DoctorSchedule> aSchedules = schedules.stream().filter(sch -> sch.getDayOfWeek() == (day + 2) % 7).collect(Collectors.toList());
        Map tMap = tSchedules.stream().collect(Collectors.toMap(t -> t.getDoctorName(), t -> t.getWorkload()));
        Map afterMap = aSchedules.stream().collect(Collectors.toMap(at -> at.getDoctorName(), at -> at.getWorkload()));

        redisUtils.hmset(tomorrow,tMap, Const.HOURS_TIME);
        redisUtils.hmset(afterTomorrow,afterMap,Const.HOURS_TIME);
    }
}