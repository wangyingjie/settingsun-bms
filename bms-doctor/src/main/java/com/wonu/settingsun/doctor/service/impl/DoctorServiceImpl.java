package com.wonu.settingsun.doctor.service.impl;

import com.wonu.settingsun.doctor.service.DoctorService;
import org.springframework.stereotype.Service;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DoctorServiceImpl
 * @date 2023/11/9 15:20
 * @description
 */
@Service
public class DoctorServiceImpl implements DoctorService {
}