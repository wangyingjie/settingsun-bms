package com.wonu.settingsun.doctor.service;

import com.wonu.settingsun.domain.entity.DoctorSchedule;

import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ScheduleService
 * @date 2023/11/9 16:21
 * @description
 */
public interface ScheduleService {

    List<DoctorSchedule> findAllByDate(Date date);

    Map<String, Integer> findSchedulesInRedisByDate(Date date);

    public void warmUpSchedule();
}