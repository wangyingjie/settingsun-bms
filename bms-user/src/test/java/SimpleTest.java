import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.utils.EncryptUtils;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName SimpleTest
 * @date 2023/11/3 16:03
 * @description
 */
public class SimpleTest {
    public static void main(String[] args) {
        System.out.println(encryptTest("111222"));
    }

    public static String encryptTest(String str) {
        String encrypt = "";
        try {
             encrypt = EncryptUtils.encrypt(str);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return encrypt;
    }

}