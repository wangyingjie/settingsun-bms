package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.user.service.RoleService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RoleServiceImplTest
 * @date 2023/11/2 14:09
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class RoleServiceImplTest {

    @Resource
    private RoleService roleServiceTest;

    @Test
    public void findAllRoles() {
        roleServiceTest.findAllRoles();
    }
}