package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.domain.vo.MenuVo;
import com.wonu.settingsun.user.service.MenuService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MenuServiceImplTest
 * @date 2023/11/6 21:39
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class MenuServiceImplTest {

    @Resource
    private MenuService menuService;
    @Test
    public void findMenuVosByName() {

        List<MenuVo> menuVos = menuService.findMenuVosByName("staff001");
        for (MenuVo menuVo : menuVos) {
            System.out.println(menuVo);
        }
    }
}