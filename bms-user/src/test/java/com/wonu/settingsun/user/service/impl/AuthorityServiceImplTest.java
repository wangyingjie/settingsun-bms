package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.domain.entity.Authority;
import com.wonu.settingsun.user.service.AuthorityService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AuthorityServiceImplTest
 * @date 2023/11/6 20:55
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AuthorityServiceImplTest {

    @Resource
    private AuthorityService authorityService;

    @Test
    public void findAllByUsername() {

        List<Authority> authorities = authorityService.findAllByUsername("staff001");
        for (Authority authority : authorities) {
            System.out.println(authority);
        }
    }
}