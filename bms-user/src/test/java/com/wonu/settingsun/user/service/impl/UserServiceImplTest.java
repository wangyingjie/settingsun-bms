package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.domain.entity.User;
import com.wonu.settingsun.user.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserServiceImplTest
 * @date 2023/11/2 11:53
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class UserServiceImplTest {

    @Resource
    private UserService userService;

    @Resource
    private RedisTemplate<String,Object> redisTemplate;

    @Resource
    private RedisUtils redisUtils;

    @Test
    public void findAllUsers() {
        List<User> users = userService.findAllUsers();
        for (User u:users) {
            System.out.println(u);
        }
    }

    @Test
    public void login() {
        userService.login("admin","123456");
    }

    @Test
    public void utilsTest() {


    }

}