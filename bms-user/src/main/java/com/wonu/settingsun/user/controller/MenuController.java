package com.wonu.settingsun.user.controller;

import com.auth0.jwt.JWT;
import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.user.service.MenuService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MenuController
 * @date 2023/11/6 21:08
 * @description
 */
@RestController
@RequestMapping("/api/menu")
public class MenuController {

    @Resource
    private MenuService menuService;

    @GetMapping("/list")
    public HttpResp<List> ListMenu(HttpServletRequest request) {

        String token = request.getHeader("token");
        String username = JWT.decode(token).getClaim("username").asString();

        return HttpResp.success(200,"菜单获取成功",menuService.findMenuVosByName(username));
    }
}