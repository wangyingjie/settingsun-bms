package com.wonu.settingsun.user;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserApp
 * @date 2023/11/2 11:43
 * @description  User模块启动类，启动时先将redis与数据库中数据统一
 *
 */
@EnableCaching
@SpringBootApplication
//要跨模块注入，需扩大扫描范围，默认是启动类所在的包及其子包
//@ComponentScan(basePackages = {"com.wonu.settingsun.user", "com.wonu.settingsun.common", "com.wonu.settingsun.log"})
@ComponentScan(basePackages = {"com.wonu.settingsun"})
//使用Mybatis的MapperScan单独对Mapper进行扫描，完成Mapper注入（在@Mapper不起作用时使用）
@MapperScan(basePackages = {"com.wonu.settingsun.logs.dao", "com.wonu.settingsun.user.dao"})
public class UserApp  {

    public static void main(String[] args) {
        SpringApplication.run(UserApp.class,args);

    }

}