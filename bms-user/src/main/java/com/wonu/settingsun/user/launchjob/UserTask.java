package com.wonu.settingsun.user.launchjob;

import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.utils.BloomUtils;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.domain.entity.User;
import com.wonu.settingsun.user.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserTask
 * @date 2023/11/2 19:07
 * @description   该模块的启动业务
 */
@Component
@Slf4j
public class UserTask implements CommandLineRunner {

    @Resource
    private RedisTemplate<String, Object> redisTemplate;
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private BloomUtils bloomUtils;
    @Resource
    private UserService userService;
    /**
     * @author nameless
     * @date 2023/11/2 20:00
     * @param args
     * @return void
     * @description 启动时加载最新的用户列表
     */
    @Override
    public void run(String... args) throws Exception {

        log.debug("执行UserTask");
        final String HASH_KEY = "users";

        List<User> users = userService.findAllUsers();
        //redis 预加载
        //list 转化为map key : user:1 value: user对象（redis中以json字符串形式存储） , 重复的key，值会覆盖，没有的会新增
        Map<String, Object> userMap = users.stream().collect(Collectors.toMap(user ->  user.getUsername(), user -> user));
        redisUtils.hmset(HASH_KEY, userMap, Const.DAY_TIME);



        // 布隆过滤器预加载
//        users.forEach(user -> {
//            // execute参数：DefaultRedisScript内部类，lua脚本字符串，返回值类型
//            String username = user.getUsername();
//
//            Long whiteUsers = redisTemplate.opsForValue().getOperations()
//                    .execute(new DefaultRedisScript<>("return redis.call('bf.add', KEYS[1], ARGV[1])", Long.class)
//                            , new ArrayList<String>() {{
//                                add("whiteUsers");
//                            }}
//                            , username);
//        });
        List<String> names = users.stream().map(User::getUsername).collect(Collectors.toList());
        bloomUtils.refreshBloom("whiteUsers",names);
    }
}