package com.wonu.settingsun.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.RoleAuthority;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RoleAuthorityDao
 * @date 2023/11/4 10:49
 * @description
 */
@Mapper
public interface RoleAuthorityDao extends BaseMapper<RoleAuthority> {
}
