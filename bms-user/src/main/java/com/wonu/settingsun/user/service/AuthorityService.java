package com.wonu.settingsun.user.service;

import com.wonu.settingsun.domain.entity.Authority;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AuthorityService
 * @date 2023/11/6 20:36
 * @description
 */
public interface AuthorityService {

    List<Authority> findAllByUsername(String username);
}
