package com.wonu.settingsun.user.service;

import com.wonu.settingsun.domain.vo.MenuVo;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MenuService
 * @date 2023/11/6 21:14
 * @description
 */
public interface MenuService {

    List<MenuVo> findMenuVosByName(String username);
}
