package com.wonu.settingsun.user.service;

import com.wonu.settingsun.domain.entity.User;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserService
 * @date 2023/11/2 11:47
 * @description
 */
public interface UserService {

    List<User> findAllUsers();

    User login(String username, String password);

//    User getUserByName(String username);

    void register(User user);

    void changePassword(String username, String pass, String passNew);

    void logOff(String username);

    void payFromAccount(String username, BigDecimal cost);

    void Recharge(String username,BigDecimal money);
}
