package com.wonu.settingsun.user.controller;

import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogTestController
 * @date 2023/11/14 15:32
 * @description  日志测试
 */
@Api(tags = "logback测试")
@RestController("/api/logT")
@Slf4j
public class LogTestController {

    @GetMapping("test")
    public HttpResp LogTest() {


        log.info("这是info级别");
        log.debug("这是debug级别");
        log.warn("这是warning级别");
        log.error("这是error级别");

        return HttpResp.success(RespCode.OK, null);
    }

}