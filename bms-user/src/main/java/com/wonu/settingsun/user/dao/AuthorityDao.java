package com.wonu.settingsun.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.Authority;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AuthorityDao
 * @date 2023/11/6 20:37
 * @description
 */
@Mapper
public interface AuthorityDao extends BaseMapper<Authority> {
}
