package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.domain.entity.Role;
import com.wonu.settingsun.user.dao.RoleDao;
import com.wonu.settingsun.user.service.RoleService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RoleServiceImpl
 * @date 2023/11/2 14:07
 * @description
 */
@Service
public class RoleServiceImpl implements RoleService {

    @Resource
    private RoleDao roleDao;
    @Override
    public List<Role> findAllRoles() {

        return roleDao.selectList(null);
    }
}