package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.common.exception.InputParameterException;
import com.wonu.settingsun.common.exception.RegexException;
import com.wonu.settingsun.common.result.ErrorCode;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.common.utils.StringValidateUtils;
import com.wonu.settingsun.user.service.ImageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ImageServiceImpl
 * @date 2023/11/7 12:03
 * @description
 */
@Service
public class ImageServiceImpl implements ImageService {

    @Resource
    private RedisUtils redisUtils;

    /**
     * @author nameless
     * @date 2023/11/7 12:08
     * @param request
     * @param captchaCode
     * @return void
     * @description   校验验证码
     */
    @Override
    public boolean verifyCode(HttpServletRequest request, String captchaCode) {

        String browserId = "";
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("browserId")) {
                    browserId = cookie.getValue();
                }
            }
        }
        String code = redisUtils.get("browser:" + browserId);
        if(!StringValidateUtils.hasText(code)) throw new RegexException(ErrorCode.CAPTCHACODETIMEOUT);
        if(!"".equals(code) && !"".equals(captchaCode) && code.equals(captchaCode)) {
            return true;
        }else {
            throw new InputParameterException(ErrorCode.CAPTCHACODEERROR);
        }
    }
}