package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.domain.entity.Authority;
import com.wonu.settingsun.domain.vo.MenuVo;
import com.wonu.settingsun.user.service.AuthorityService;
import com.wonu.settingsun.user.service.MenuService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName MenuServiceImpl
 * @date 2023/11/6 21:15
 * @description
 */
@Service
public class MenuServiceImpl implements MenuService {

    @Resource
    private AuthorityService authorityService;
    @Override
    public List<MenuVo> findMenuVosByName(String username) {

        List<Authority> authorities = authorityService.findAllByUsername(username);

        /*使用递归调用进行MenuVo的封装*/
        if (Objects.nonNull(authorities)) {
            List<MenuVo> menuVos = new ArrayList<>();
            // 找到所有顶级菜单（level为1）
            List<Authority> topLevelAuthorities = authorities.stream()
                    .filter(authority -> authority.getLevel() == 1)
                    .collect(Collectors.toList());
            // 遍历顶级菜单并递归处理子菜单
            for (Authority authority : topLevelAuthorities) {
                MenuVo menuVo = buildMenuVo(authority, authorities);
                menuVos.add(menuVo);
            }

            return menuVos;
        }

        return null;
    }

    /**
     * @param authority  调用方法时，作为父级菜单的权限对象
     * @param authorities 全部权限集合
     * @return MenuVo
     * @author nameless
     * @date 2023/9/19 14:54
     * @description 递归将权限Authority封装成MenuVo
     *              注意递归的调用对象，每次会进行转变；
     *              结束递归的条件：参与递归的节点没有子节点
     */
    private MenuVo buildMenuVo(Authority authority, List<Authority> authorities) {
        //当前Authority 进行其他属性封装
        MenuVo menuVo = new MenuVo();
        menuVo.setId(authority.getAuthorityId());
//        menuVo.setPath(String.valueOf(authority.getSort()));
        menuVo.setPath(authority.getSort());
        menuVo.setTitle(authority.getPname());
        menuVo.setIcon(authority.getIcon());
        menuVo.setLinkUrl(authority.getUrl());

        // 递归处理子菜单
        // 遍历Authorities 获取当前Authority对象的子菜单
        List<Authority> childAuthorities = authorities.stream()
                .filter(au -> au.getParentId() == authority.getAuthorityId())
                .collect(Collectors.toList());

        /*若存在子节点，则创建一份List对象，对于每一个子节点进行buildMenuVo封装，将返回的封装类添加到list中
         *并进行setChildren赋值
         * 若不存在子节点，则本节点的children属性为null
         */
        if (childAuthorities.size() > 0) {
            List<MenuVo> children = new ArrayList<>();
            for (Authority childAuthority : childAuthorities) {
                MenuVo childMenuVo = buildMenuVo(childAuthority, authorities);
                children.add(childMenuVo);
            }
            menuVo.setChildren(children);
        }

        return menuVo;
    }
}