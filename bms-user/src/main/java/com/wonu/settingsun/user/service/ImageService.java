package com.wonu.settingsun.user.service;

import javax.servlet.http.HttpServletRequest;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ImageService
 * @date 2023/11/6 10:51
 * @description
 */
public interface ImageService {

    boolean verifyCode(HttpServletRequest request, String captchaCode);
}
