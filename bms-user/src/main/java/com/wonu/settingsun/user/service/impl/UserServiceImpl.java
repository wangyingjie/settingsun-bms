package com.wonu.settingsun.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wonu.settingsun.common.exception.*;
import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.result.ErrorCode;
import com.wonu.settingsun.common.utils.BloomUtils;
import com.wonu.settingsun.common.utils.EncryptUtils;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.common.utils.StringValidateUtils;
import com.wonu.settingsun.domain.entity.User;
import com.wonu.settingsun.user.dao.UserDao;
import com.wonu.settingsun.user.service.UserService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserServiceImpl
 * @date 2023/11/2 11:49
 * @description
 */
@Service
public class UserServiceImpl implements UserService {

    @Resource
    private UserDao userDao;
    @Resource
    private RedisTemplate<String, Object> redisTemplate;

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private BloomUtils bloomUtils;

    @Override
    public List<User> findAllUsers() {

        return userDao.selectList(null);
    }

    @Override
    public User login(String username, String password) {


        if(!checkForBloom(username)) throw new AuthenticationException(ErrorCode.USERNAMENOTFOUND);

        User user = null;

        if(redisTemplate.opsForHash().hasKey("users",username)) {
            user = (User)redisUtils.hget("users", username);
            String decryptPass = "";
            try {
                decryptPass = EncryptUtils.decrypt(user.getPassword());
            } catch (Exception e) {
                throw new EncryptException(ErrorCode.DECRYPTERROR);
            }
            if(!password.equals(decryptPass)) {
                throw new InputParameterException(ErrorCode.USERNAMEORPASSWORDERROR);
            }

        }else {
            String encryptPass = "";
            try {
                encryptPass = EncryptUtils.encrypt(password);
            } catch (Exception e) {
                throw new EncryptException(ErrorCode.ENCRYPTERROR);
            }
            user = userDao.selectOne(new QueryWrapper<User>().eq("username", username).eq("password", encryptPass));
            if(Objects.isNull(user)){
                throw new InputParameterException(ErrorCode.USERNAMEORPASSWORDERROR);
            }else{
                //添加到缓存
                redisUtils.hset("users",username,user, Const.DAY_TIME);
            }
        }
        return user;
    }

/*    @Cacheable(cacheNames = "users",key ="#username", cacheManager ="redisCacheManager")
    public User getUserByName(String username) {
        return userDao.selectOne(new QueryWrapper<User>().eq("username", username));
    }*/

    /**
     * @author nameless
     * @date 2023/11/6 15:22
     * @param user
     * @return void
     * @description 注册用户信息，username、password、roleId必填
     */
    @Override
    public void register(User user) {
        String username = user.getUsername();
        String password = user.getPassword();
        String nikeName = user.getNikeName();

        if(checkForBloom(username)) throw new AuthenticationException(ErrorCode.DUPLICATE);

//        if(!StringValidateUtils.passwordRegexCheck(password)) throw new RegexException(ErrorCode.PASSWOERDREGEX);
        if(!StringValidateUtils.hasText(nikeName)) {
            Random rd = new Random();
            nikeName = "user#" + rd.nextInt(10000);
            user.setNikeName(nikeName);
        }
        if(Objects.isNull(user.getBalance())) {

            BigDecimal decimal = new BigDecimal("0.0");
            user.setBalance(decimal);
        }


        //对密码进行加密再存储到数据库中
        String encryptPass = null;
        try {
            encryptPass = EncryptUtils.encrypt(password);
        } catch (Exception e) {
            throw new EncryptException(ErrorCode.ENCRYPTERROR);
        }
        user.setPassword(encryptPass);
        userDao.insert(user);

        redisUtils.hset("users",username,user);
        bloomUtils.addToBloom("whiteUsers", username);
    }

    /**
     * @author nameless
     * @date 2023/11/6 15:41
     * @param username
     * @param pass
     * @param passNew
     * @return void
     * @description 修改密码,redis同步更新
     */
    @Override
    public void changePassword(String username, String pass, String passNew) {

        if(!checkForBloom(username)) throw new AuthenticationException(ErrorCode.USERNAMENOTFOUND);
        User user = userDao.selectOne(new QueryWrapper<User>().eq("username", username).eq("password", pass));
        if(Objects.isNull(user)) throw new AuthenticationException(ErrorCode.USERNAMEORPASSWORDERROR);
        user.setPassword(passNew);
        userDao.updateById(user);

        redisUtils.hset("users",username,user);

    }

    /**
     * @author nameless
     * @date 2023/11/6 15:50
     * @param username
     * @return void
     * @description 注销账户 删除数据库、redis、布隆白名单
     */
    @Override
    public void logOff(String username) {

        if(!StringValidateUtils.hasText(username)) throw new InputParameterException(ErrorCode.USERNAMEISENPTY);
        userDao.delete(new QueryWrapper<User>().eq("username", username));

        //更新redis、布隆
        redisUtils.deleteHashKey("users", username);
        List<String> usernames = userDao.selectList(null).stream().map(User::getUsername).collect(Collectors.toList());
        bloomUtils.refreshBloom("whiteUsers",usernames);


    }

    /**
     * @author nameless
     * @date 2023/11/10 16:34
     * @param cost
     * @return void
     * @description 从账户上支付订单
     */
    @Override
    public void payFromAccount(String username, BigDecimal cost) {


        //1.先从redis中获取，看有没有过期(暂时放弃)




        User user = userDao.selectOne(new QueryWrapper<User>().eq(StringUtils.hasText(username), "username", username));
        BigDecimal balance = user.getBalance();
        if(balance.compareTo(cost) < 0) throw new BmsException(500, "当前余额不足,请前往充值");
        BigDecimal subtract = balance.subtract(cost);
        user.setBalance(subtract);
        userDao.updateById(user);
    }

    /**
     * @author nameless
     * @date 2023/11/10 16:55
     * @param username
     * @param money
     * @return void
     * @description 充值
     */
    @Override
    public void Recharge(String username,BigDecimal money) {

        User user = userDao.selectOne(new QueryWrapper<User>().eq(StringUtils.hasText(username), "username", username));
        BigDecimal balance = user.getBalance();
        user.setBalance(money.add(balance));
        userDao.updateById(user);
    }


    /**
     * @author nameless
     * @date 2023/11/2 21:16
     * @param username
     * @return boolean
     * @description 布隆过滤器，不存在返回false，可能存在返回true
     */
    private boolean checkForBloom(String username) {

        Long value = (Long)redisTemplate.opsForValue().getOperations()
                .execute(new DefaultRedisScript<>("return redis.call('bf.exists',KEYS[1],ARGV[1])",Long.class)
                        , new ArrayList<String>() {{
                            add("whiteUsers");
                        }}
                        , username);

        return value == 1;
    }
}