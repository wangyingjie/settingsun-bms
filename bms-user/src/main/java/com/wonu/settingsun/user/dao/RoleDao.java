package com.wonu.settingsun.user.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.Role;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RoleDao
 * @date 2023/11/2 14:08
 * @description
 */
@Mapper
public interface RoleDao extends BaseMapper<Role> {
}