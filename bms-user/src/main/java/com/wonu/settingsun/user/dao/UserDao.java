package com.wonu.settingsun.user.dao;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.User;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName UserDao
 * @date 2023/11/2 11:49
 * @description
 */
@Mapper
public interface UserDao extends BaseMapper<User> {

}
