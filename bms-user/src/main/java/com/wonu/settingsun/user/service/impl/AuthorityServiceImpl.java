package com.wonu.settingsun.user.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.wonu.settingsun.domain.entity.Authority;
import com.wonu.settingsun.domain.entity.RoleAuthority;
import com.wonu.settingsun.domain.entity.User;
import com.wonu.settingsun.user.dao.AuthorityDao;
import com.wonu.settingsun.user.dao.RoleAuthorityDao;
import com.wonu.settingsun.user.dao.UserDao;
import com.wonu.settingsun.user.service.AuthorityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AuthorityServiceImpl
 * @date 2023/11/6 20:36
 * @description
 */
@Service
public class AuthorityServiceImpl implements AuthorityService {

    @Resource
    private AuthorityDao authorityDao;

    @Resource
    private UserDao userDao;
    @Resource
    private RoleAuthorityDao roleAuthorityDao;

    /**
     * @author nameless
     * @date 2023/11/6 20:39
     * @param username
     * @return List<Authority>
     * @description 通过用户名获取其可以访问的所有菜单
     */
    @Override
    public List<Authority> findAllByUsername(String username) {

        User user = userDao.selectOne(new QueryWrapper<User>().eq("username", username));

        //Role role = roleDao.selectOne(new QueryWrapper<Role>().eq("role_id", user.getRoleId()));
        List<RoleAuthority> raList = roleAuthorityDao.selectList(
                new QueryWrapper<RoleAuthority>().eq("role_id", user.getRoleId()));

        List<Authority> authorities = raList.stream().map(roleAuthority ->
            authorityDao.selectOne(new QueryWrapper<Authority>()
                    .eq("authority_id", roleAuthority.getAuthorityId()))
        ).collect(Collectors.toList());

        return authorities;
    }
}