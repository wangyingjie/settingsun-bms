package com.wonu.settingsun.user.controller;

import cn.hutool.captcha.CaptchaUtil;
import cn.hutool.captcha.LineCaptcha;
import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.common.utils.RedisUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.UUID;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ImageController
 * @date 2023/11/6 10:53
 * @description   图片相关处理类
 */
@RestController
@Api(tags = "图片模块")
@RequestMapping("/api/img")
public class ImageController {

    @Resource
    private RedisUtils redisUtils;


    /**
     * @author nameless
     * @date 2023/11/6 11:22
     * @param response
     * @return HttpResp
     * @description 页面初始时，加载验证码图片，生成浏览器id并放入redis中
     */
    @ApiOperation(value = "code",notes = "初始加载验证码")
    @GetMapping("/code")
    public void getCode(HttpServletResponse response) throws IOException {

        response.setContentType("image/jpeg");
        response.setHeader("Pragma", "no-cache");
        //定义图形验证码的长、宽、验证码字符数、干扰线宽度
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(100, 50, 4, 4);
        String code = lineCaptcha.getCode();


        String browserId = UUID.randomUUID().toString();
        // 添加到cookie中应当在响应输出流之前
        Cookie cookie = new Cookie("browserId", browserId);
        cookie.setPath("/api");
        response.addCookie(cookie);
        //存储到redis中，验证时从redis中去取，先拿浏览器标记，再按标记取验证码与用户输入比较（加入标记是为了考虑多用户情况）
        redisUtils.set("browser:" + browserId,code, Const.MINUTE_TIME);
        //图形验证码写出，可以写出到文件，也可以写出到流
        lineCaptcha.write(response.getOutputStream());
        response.getOutputStream().close();


        //返回了验证码图片，就不应该再返回HttpResp结构体了
        //return HttpResp.success(RespCode.OK, "验证码发送成功");
    }

    /**
     * @author nameless
     * @date 2023/10/26 17:46
     * @param response
     * @return HttpResp
     * @description 刷新验证码
     */
    @GetMapping("/flush")
    public void flushCode(HttpServletRequest request, HttpServletResponse response) throws IOException {

        //String browserId = request.getHeader("browserId");
        String browserId = "";
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (cookie.getName().equals("browserId")) {
                    browserId = cookie.getValue();
                }
            }
        }
        LineCaptcha lineCaptcha = CaptchaUtil.createLineCaptcha(100, 50, 4, 4);
        String code = lineCaptcha.getCode();
        //返回给前端
        lineCaptcha.write(response.getOutputStream());
        //更新验证码 若redis过期了，则创建该key-value
        redisUtils.set("browser:" + browserId,code, Const.MINUTE_TIME);
//        return HttpResp.success(RespCode.OK,"更新验证码成功");
    }


}