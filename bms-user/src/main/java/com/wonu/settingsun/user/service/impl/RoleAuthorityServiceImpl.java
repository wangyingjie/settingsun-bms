package com.wonu.settingsun.user.service.impl;

import com.wonu.settingsun.user.dao.RoleAuthorityDao;
import com.wonu.settingsun.user.service.RoleAuthorityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RoleAuthorityServiceImpl
 * @date 2023/11/4 10:48
 * @description
 */
@Service
public class RoleAuthorityServiceImpl implements RoleAuthorityService {

    @Resource
    private RoleAuthorityDao roleAuthorityDao;
}