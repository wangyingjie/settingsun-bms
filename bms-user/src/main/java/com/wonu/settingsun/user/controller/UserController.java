package com.wonu.settingsun.user.controller;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.wonu.settingsun.common.exception.EncryptException;
import com.wonu.settingsun.common.exception.InputParameterException;
import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.result.ErrorCode;
import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.common.utils.EncryptUtils;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.common.utils.StringValidateUtils;
import com.wonu.settingsun.domain.dto.UserFormDto;
import com.wonu.settingsun.domain.dto.UserRegisterDto;
import com.wonu.settingsun.domain.entity.User;
import com.wonu.settingsun.logs.annotation.SysLog;
import com.wonu.settingsun.user.service.ImageService;
import com.wonu.settingsun.user.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;


/**
 * @author nameless
 * @version 1.0
 * @ClassName UserController
 * @date 2023/11/2 15:05
 * @description
 */
@Api(tags = "用户模块")
@RestController
@RequestMapping("/api/user")
@Slf4j
@Validated  // Controller形参中进行校验
public class UserController {

    @Resource
    private UserService userService;
    @Resource
    private RedisUtils redisUtils;
    @Resource
    private ImageService imageService;

    @ApiOperation(value = "查询所有", notes = "findAll")
//    @Role(values = Const.COMMON_AREA)
    @GetMapping("findAll")
    public HttpResp<List> findAll() {
        List<User> users = userService.findAllUsers();
       return HttpResp.success(RespCode.OK, users);
    }

    /**
     * @author nameless
     * @date 2023/11/8 15:44
     * @param roleId
     * @return HttpResp<List>
     * @description 从redis中获取指定角色的账号集合
     */
    @ApiOperation(value = "获取指定角色的用户账号", notes = "findUsersByRole")
    @GetMapping("/findUsersByRole")
    public HttpResp<List> findUsersByRole(Long roleId) {
        List<String> usernaems = redisUtils.hEntries("users").entrySet().stream()
                .map(en -> (User) en.getValue())
                .filter(user -> user.getRoleId().equals(roleId))
                .map(User::getUsername).collect(Collectors.toList());
        return HttpResp.success(RespCode.OK, usernaems);
    }

    /**
     * @author nameless
     * @date 2023/11/6 14:25
     * @param userForm
     * @return HttpResp<String>
     * @description jwt形式的登录（验证码还未添加），jwt存储在cookie和redis中,并返回给前端
     */
    @SysLog("登录操作")
    @PostMapping("login")
//    @CrossOrigin(originPatterns = "*",allowCredentials="true",allowedHeaders = "*")
    public HttpResp<String> login(@Validated @RequestBody UserFormDto userForm, HttpServletRequest request, HttpServletResponse response) {

        String username = userForm.getUsername();
        String password = userForm.getPassword();
        String code = userForm.getCode();

        //校验验证码
        imageService.verifyCode(request,code);

        User user = userService.login(username, password);

        final String salt = username;

        String jwt = JWT.create().withClaim("username",user.getUsername())
                .withClaim("roleId",user.getRoleId())
                .withClaim("nikeName", user.getNikeName())
                .withClaim("userId", user.getUserId())
                .withExpiresAt(new Date(System.currentTimeMillis() + Const.MINUTES_TIME))
                .sign(Algorithm.HMAC256(username));
        log.debug("生成的token: {}",jwt);

        //加密salt
        String saltKey = "";
        try {
             saltKey = EncryptUtils.encrypt(salt);
        } catch (Exception e) {
            throw new EncryptException(ErrorCode.ENCRYPTERROR);
        }
        redisUtils.set(jwt,saltKey,Const.MINUTES_TIME * 2);

        Cookie cookie = new Cookie("token", jwt);
        cookie.setPath("/user");
        response.addCookie(new Cookie("token", jwt));

        return HttpResp.success(200, "登录成功", jwt);
    }

    /**
     * @author nameless
     * @date 2023/11/8 9:46
     * @param registerDto
     * @return HttpResp
     * @description 注册使用mq进行异步
     */

    @SysLog("注册账户")
    @PostMapping("/register")
    public HttpResp register(@Validated @RequestBody UserRegisterDto registerDto) {

        if (!registerDto.getPasswordRe().equals(registerDto.getPassword())) {
            throw new InputParameterException(503, "两次密码不一致");
        }

        User user = new User();
        user.setUsername(registerDto.getUsername());
        user.setPassword(registerDto.getPassword());
        user.setNikeName(registerDto.getNikeName());
        user.setBalance(registerDto.getBalance());
        user.setRoleId(registerDto.getRoleId());
        user.setCreateTime(new Date(System.currentTimeMillis()));

        userService.register(user);

        return HttpResp.success(200,"注册成功",null);
    }

    @SysLog("修改密码")
    @PutMapping("/changePassword")
    public HttpResp changePassword(@NotBlank(message = "账户不能为空") @Pattern(regexp = StringValidateUtils.NAME_REGEX,message = "账户必须由3-15位字母、数字、下划线") String username,
                                   @NotBlank(message = "密码不能为空")  String pass, String passNew) {

        userService.changePassword( username,  pass,  passNew);
        return HttpResp.success(RespCode.OK, "修改成功");
    }

    /**
     * @author nameless
     * @date 2023/11/8 10:09
     * @param username
     * @return HttpResp
     * @description 注销账户
     */
    @DeleteMapping("logOff")
    @SysLog("注销账号")
    public HttpResp logOff(String username) {

        userService.logOff(username);
        return HttpResp.success(RespCode.OK,"注销成功");
    }

    /**
     * @author nameless
     * @date 2023/11/6 16:22
     * @param request
     * @param response
     * @return HttpResp
     * @description 登出删除Cookie和redis中的jwt
     */
    @SysLog("登出账号")
    @DeleteMapping("logOut")
    public HttpResp logOut(HttpServletRequest request,HttpServletResponse response) {

        String jwt = "";
        for (Cookie cookie : request.getCookies()) {
            if("token".equals(cookie.getName())) {
                jwt = cookie.getValue();
            }
        }

        Cookie newCookie=new Cookie("token","");
        newCookie.setMaxAge(0);
        response.addCookie(newCookie);
        redisUtils.deleteKey(jwt);

        return HttpResp.success(RespCode.OK,"登出成功");
    }

    @PutMapping("/pay")
    @ApiOperation("支付订单")
    public HttpResp payForOrder(String username, double cost) {

        BigDecimal decimal = BigDecimal.valueOf(cost);
        userService.payFromAccount(username, decimal);


        return HttpResp.success(RespCode.OK, "支付成功");
    }

}