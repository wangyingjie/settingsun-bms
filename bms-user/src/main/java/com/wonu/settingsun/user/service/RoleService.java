package com.wonu.settingsun.user.service;

import com.wonu.settingsun.domain.entity.Role;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName RoleService
 * @date 2023/11/2 14:06
 * @description
 */
public interface RoleService {
    List<Role> findAllRoles();
}
