/*
给axios的二次封装，主要的目的是添加请求以及响应拦截器
在这里request就是axios的一个实例，作用跟axios一样
*/
var request = axios.create({
    /*当前请求的跟路径*/
    //baseUrl: "http://localhost:9010",
    timeout: 5000
});
/*给request添加请求拦截器*/
request.interceptors.request.use(config => {
    //获取当前请求的路径
    var requestPath = config.url;// /login
    //console.log("请求拦截器获得的路径：", requestPath)
    if(requestPath !== "/api/user/login") {
        //除了login请求之外，所有的请求添加token
        var token = localStorage.getItem("token");
        if(token) {
            //如果token不为空给请求头添加token
            config.headers["token"] = token;
        }
    }
    return config;
},error => {
    /*如果请求异常中断请求*/
    return Promise.reject(error);
});

/*响应拦截器*/
request.interceptors.response.use(data => {
    console.log("响应拦截器获取到的返回值：", data);
    var code = data.data.code;
    //console.log("响应拦截器获取到的返回值code：", code)
    if(code === 600 || code === 601 || code === 602 || code === 603) {
        alert(data.data.message)
        //跳转到登陆页面
        parent.location.href = "/index.html";
    }
    return data;
},error=>{
    /*如果请求异常中断请求*/
    return Promise.reject(error);
})