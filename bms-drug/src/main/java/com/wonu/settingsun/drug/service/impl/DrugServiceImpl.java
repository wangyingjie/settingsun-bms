package com.wonu.settingsun.drug.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wonu.settingsun.common.exception.BmsException;
import com.wonu.settingsun.common.utils.StringValidateUtils;
import com.wonu.settingsun.domain.entity.Drug;
import com.wonu.settingsun.domain.entity.ElderlyHealthInfo;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.drug.dao.DrugDao;
import com.wonu.settingsun.drug.service.DrugService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.List;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DrugServiceImpl
 * @date 2023/11/11 15:32
 * @description
 */
@Service
public class DrugServiceImpl implements DrugService {

    @Resource
    private DrugDao drugDao;
    @Override
    public PageVo<List> findAllByPage(int currentPage, int pageSize, String drugName, String type) {
        IPage<Drug> ipage = new Page<>();
        ipage.setCurrent(currentPage);
        ipage.setSize(pageSize);

        QueryWrapper<Drug> wrapper = new QueryWrapper<>();
        wrapper.eq(StringUtils.hasText(type),"type", type)
                .like(StringValidateUtils.hasText(drugName), "name","%" + drugName +"%");
        IPage<Drug> iPage = drugDao.selectPage(ipage, wrapper);

        PageVo<List> pageVo = new PageVo<>();
        pageVo.setData(iPage.getRecords());
        pageVo.setTotal(iPage.getTotal());
        //返回当前页的记录数
        return pageVo;
    }

    @Override
    public void addDrug(Drug drug) {

        int row = drugDao.insert(drug);
        if(row <= 0) throw new BmsException(500,"添加药品记录失败");
    }

    @Override
    public void update(Drug drug) {


        int row = drugDao.updateById(drug);
        if(row <= 0) throw new BmsException(500, "修改药品记录失败");

    }

    @Override
    public void removeDrugById(Long id) {
        int row = drugDao.deleteById(id);
        if(row <= 0) throw new BmsException(500, "通过id删除记录失败");
    }
}