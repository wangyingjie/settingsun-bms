package com.wonu.settingsun.drug.utils;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * ClassName： Constant
 *
 * @author nameless
 * Description： 常量类 ，从yml配置中读取值
 * @date 2023/9/13 15:16
 */
@Component
public class Constant {

    @Value("${qiNiu.accessKey}")
    private String qiNiu_accessKey;
    @Value("${qiNiu.secretKey}")
    private String qiNiu_secretKey;
    @Value("${qiNiu.bucket}")
    private String qiNiu_bucket;
    @Value("${qiNiu.bucketUrl}")
    private String qiNiu_bucketUrl;

    public static String QINIU_ACCESSKEY;
    public static String QINIU_SECRETKEY;
    public static String QINIU_BUCKET;
    public static String QINIU_BUCKETURL;


    @PostConstruct
    public void init() {
        QINIU_ACCESSKEY = qiNiu_accessKey;
        QINIU_SECRETKEY = qiNiu_secretKey;
        QINIU_BUCKET = qiNiu_bucket;
        QINIU_BUCKETURL = qiNiu_bucketUrl;
    }
}