package com.wonu.settingsun.drug;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DrugApp
 * @date 2023/11/11 14:58
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.wonu.settingsun")
public class DrugApp {
    public static void main(String[] args) {
        SpringApplication.run(DrugApp.class,args);
    }
}