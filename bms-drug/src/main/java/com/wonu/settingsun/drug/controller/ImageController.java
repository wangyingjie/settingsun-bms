package com.wonu.settingsun.drug.controller;

import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.drug.utils.QiNuUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.UUID;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ImageController
 * @date 2023/11/11 14:40
 * @description
 */
@RestController
@RequestMapping("/api/image")
public class ImageController {

    /**
     * @author nameless
     * @date 2023/9/13 14:43
     * @param
     * @return Result
     * @description
     *          OOS图片上传流程：
     *              1、前端upload组件 action 发送Post请求
     *              2、后端接收 使用@Requestpart注解 和MultiFile接口接收图片或文件
     *              3、方法 拼接图片名 并使用上传工具类向OOS上传
     *              4、返回URL给前端存储 并回显
     *              5、之后再发送请求添加数据到数据库
     */
    @PostMapping("/upload")
    public HttpResp imgUpload(@RequestPart MultipartFile file) {

        /*try {
            InputStream inputStream = multipartFile.getInputStream();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }*/

        String imgName = file.getOriginalFilename();
        //生成随机拼接段
        String replace = UUID.randomUUID().toString().replace("-", "");

        imgName = replace.concat(imgName.substring(imgName.lastIndexOf(".")));

        String url = null;
        try {
            url = QiNuUtils.uploadFile(file.getBytes(), imgName);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return HttpResp.success(RespCode.OK.OK, url);
    }
}