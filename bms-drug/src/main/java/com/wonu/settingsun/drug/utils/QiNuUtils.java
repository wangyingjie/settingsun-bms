package com.wonu.settingsun.drug.utils;

import com.google.gson.Gson;
import com.qiniu.common.QiniuException;
import com.qiniu.http.Response;
import com.qiniu.storage.BucketManager;
import com.qiniu.storage.Configuration;
import com.qiniu.storage.Region;
import com.qiniu.storage.UploadManager;
import com.qiniu.storage.model.DefaultPutRet;
import com.qiniu.util.Auth;
import org.springframework.stereotype.Component;

/**
 * ClassName： QiNuUtils
 *
 * @author nameless
 * Description： 七牛云官方配置文档用法封装
 * @date 2023/9/13 15:10
 */
@Component
public class QiNuUtils {

    /**
     * 图片上传
     * @param uploadBytes
     * @param filename
     */
    public static String uploadFile(byte[] uploadBytes, String filename) {
        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.region0());
        cfg.resumableUploadAPIVersion = Configuration.ResumableUploadAPIVersion.V2;// 指定分片上传版本
        //...其他参数参考类注释
        UploadManager uploadManager = new UploadManager(cfg);
        //...生成上传凭证，然后准备上传
        String accessKey = Constant.QINIU_ACCESSKEY;
        String secretKey = Constant.QINIU_SECRETKEY;
        String bucket = Constant.QINIU_BUCKET;
        //外链接域名
        String url = Constant.QINIU_BUCKETURL;

        //默认不指定key的情况下，以文件内容的hash值作为文件名 文件的名称
        String key = filename;

        // 签名（密码）
        Auth auth = Auth.create(accessKey, secretKey);
        String upToken = auth.uploadToken(bucket);

        try {
            Response response = uploadManager.put(uploadBytes, key, upToken);
            //解析上传成功的结果
            DefaultPutRet putRet = new Gson().fromJson(response.bodyString(), DefaultPutRet.class);
            System.out.println(putRet.key);
            System.out.println(putRet.hash);
            return url + "/" + filename;

        } catch (QiniuException ex) {
            Response r = ex.response;
            System.err.println(r.toString());
            try {
                System.err.println(r.bodyString());
            } catch (QiniuException ex2) {
                //ignore
            }
        }
        return "";
    }

    /**
     * 删除文件
     *
     * @param fileName
     */
    public static void deleteFile(String fileName) {
        //构造一个带指定 Region 对象的配置类
        Configuration cfg = new Configuration(Region.region2());
        //...其他参数参考类注释
        String accessKey = Constant.QINIU_ACCESSKEY;
        String secretKey = Constant.QINIU_SECRETKEY;
        String bucket = Constant.QINIU_BUCKET;

        Auth auth = Auth.create(accessKey, secretKey);
        BucketManager bucketManager = new BucketManager(auth, cfg);
        try {
            bucketManager.delete(bucket, fileName);
        } catch (QiniuException ex) {
            //如果遇到异常，说明删除失败
            System.err.println(ex.code());
            System.err.println(ex.response.toString());
        }
    }
}