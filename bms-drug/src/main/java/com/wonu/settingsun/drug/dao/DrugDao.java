package com.wonu.settingsun.drug.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.Drug;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DrugDao
 * @date 2023/11/11 15:33
 * @description
 */
@Mapper
public interface DrugDao extends BaseMapper<Drug> {
}
