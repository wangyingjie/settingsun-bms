package com.wonu.settingsun.drug.controller;

import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.domain.dto.DrugQueryDto;
import com.wonu.settingsun.domain.entity.Drug;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.drug.service.DrugService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotBlank;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DrugController
 * @date 2023/11/11 15:59
 * @description
 */
@RestController
@RequestMapping("/api/drug")
@Api(tags = "药品管理")
@Validated
public class DrugController {

    @Resource
    private DrugService drugService;

    @ApiOperation("多条件分页")
    @PostMapping("/page")
    public HttpResp findAllByPage(int pageNum, int pageSize, @RequestBody DrugQueryDto queryDto) {


        PageVo<List> allByPage = drugService.findAllByPage(pageNum, pageSize, queryDto.getName(), queryDto.getType());

        return HttpResp.success(RespCode.OK,allByPage);
    }

    @ApiOperation("修改药品信息")
    @PutMapping("/modifyInfo")
    public HttpResp modifyInfo(@Validated @RequestBody Drug drug) {

        drugService.update(drug);

        return HttpResp.success(RespCode.OK,"修改成功");
    }

    @ApiOperation("添加药品")
    @PostMapping("/addInfo")
    public HttpResp addInfo(@Validated @RequestBody Drug drug) {

        drugService.addDrug(drug);

        return HttpResp.success(RespCode.OK,"添加成功");
    }

    @ApiOperation("删除药品")
    @DeleteMapping("/removeById/{id}")
    public HttpResp removeDrugById(@PathVariable int id) {

        return HttpResp.success(RespCode.OK,"id删除成功");
    }

}