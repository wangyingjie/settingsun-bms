package com.wonu.settingsun.drug.service;

import com.wonu.settingsun.domain.entity.Drug;
import com.wonu.settingsun.domain.vo.PageVo;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName DrugService
 * @date 2023/11/11 15:32
 * @description
 */
public interface DrugService {

    PageVo<List> findAllByPage(int currentPage, int pageSize, String drugName, String type);

    void addDrug(Drug drug);

    void update(Drug drug);

    void removeDrugById(Long id);

}
