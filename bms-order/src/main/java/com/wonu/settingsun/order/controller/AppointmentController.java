package com.wonu.settingsun.order.controller;

import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.domain.dto.AppointmentQueryDto;
import com.wonu.settingsun.domain.entity.AppointmentOrder;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.order.service.AppointmentService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;

import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AppointmentController
 * @date 2023/11/10 15:02
 * @description
 */
@RestController
@RequestMapping("/api/appointment")
@Api(tags = "预约模块")
public class AppointmentController {

    @Resource
    private RedisUtils redisUtils;

    @Resource
    private AppointmentService appointmentService;


    /**
     * @author nameless
     * @date 2023/11/10 16:30
     * @param appointmentOrder
     * @param response
     * @return HttpResp
     * @description 生成订单后会创建一个定时器，定时器key在cookie中
     */
    @ApiOperation(value = "生成订单", notes = "createAppointment")
    @PostMapping("/create")
    public HttpResp createAppointment(AppointmentOrder appointmentOrder, HttpServletResponse response) {

        appointmentService.createAppointment(appointmentOrder);

        /*//在redis中生成支付定时器
        String key = UUID.randomUUID().toString();
        response.addCookie(new Cookie("timerKey",key));
        redisUtils.set(key,"timer", Const.MINUTES_TIME);*/
        //创建完订单后使用定时器，倒计时，限用户10分钟内支付订单，否住自动取消订单，释放名额


        return HttpResp.success(RespCode.OK, "创建完成");
    }


    @GetMapping("/findAll")
    @ApiOperation(value = "查询所有预约订单", notes = "findAppointmentsByPage")
    public HttpResp findAppointmentsByPage(AppointmentQueryDto appointmentQueryDto, int pageNum, int pageSize) {

        PageVo<List> allByPage = appointmentService.findAllByPage(pageNum, pageSize, appointmentQueryDto);

        return HttpResp.success(RespCode.OK, allByPage);
    }
}