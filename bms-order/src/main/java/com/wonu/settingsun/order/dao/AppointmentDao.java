package com.wonu.settingsun.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.AppointmentOrder;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AppointmentDao
 * @date 2023/11/9 17:08
 * @description
 */
@Mapper
public interface AppointmentDao extends BaseMapper<AppointmentOrder> {
}
