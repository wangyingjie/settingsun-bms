package com.wonu.settingsun.order;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderApp
 * @date 2023/11/9 17:44
 * @description
 */
@SpringBootApplication
@EnableTransactionManagement
@ComponentScan("com.wonu.settingsun")
public class OrderApp {
    public static void main(String[] args) {
        SpringApplication.run(OrderApp.class,args);
    }
}