package com.wonu.settingsun.order.event;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.ApplicationEvent;
import org.springframework.stereotype.Component;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderCreatedEvent
 * @date 2023/11/14 17:08
 * @description  自定义事件类，用于表示订单创建成功
 */

public class OrderCreatedEvent extends ApplicationEvent {

    private Long orderId;

    public OrderCreatedEvent(Object source, Long orderId) {
        super(source);
        this.orderId = orderId;
    }

    public Long getOrderId() {
        return orderId;
    }
}