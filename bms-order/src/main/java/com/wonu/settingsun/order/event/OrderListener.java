package com.wonu.settingsun.order.event;

import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.EventListener;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderListener
 * @date 2023/11/14 19:16
 * @description   监听类
 */

@Slf4j
public class OrderListener implements ApplicationListener<OrderCreatedEvent> {




    /**
     * @author nameless
     * @date 2023/11/14 17:37
     * @param event
     * @return void
     * @description 监听预约订单是否创建，若创建则调用定时任务开启支付倒计时
     */
    @EventListener
    public void handleOrderCreatedEvent(OrderCreatedEvent event) {

        log.debug("监听到事件{}", event);
        Long orderId = event.getOrderId();
        //传递订单id
        /*appointmentTask.setOrderId(orderId);
        //执行支付倒计时
        appointmentTask.countdown();*/
    }

    @Override
    public void onApplicationEvent(OrderCreatedEvent event) {

    }
}