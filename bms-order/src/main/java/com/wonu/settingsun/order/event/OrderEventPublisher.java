package com.wonu.settingsun.order.event;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Component;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderEventPublisher
 * @date 2023/11/14 17:15
 * @description  订单事件发布类
 */

public class OrderEventPublisher implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher eventPublisher;

    public OrderEventPublisher(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    /**
     * @author nameless
     * @date 2023/11/14 17:17
     * @param orderId   插入数据库中的id
     * @return void
     * @description 发布订单创建事件
     */
    public void publishOrderCreatedEvent(Long orderId) {
        OrderCreatedEvent event = new OrderCreatedEvent(this, orderId);
        eventPublisher.publishEvent(event);
    }

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.eventPublisher = applicationEventPublisher;
    }
}
