package com.wonu.settingsun.order.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.vo.AppointmentOrderVO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.reflection.wrapper.BaseWrapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AppointmentVoDao
 * @date 2023/11/10 10:39
 * @description
 */
@Mapper
public interface AppointmentVoDao extends BaseMapper<AppointmentOrderVO> {
}
