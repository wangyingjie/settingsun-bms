package com.wonu.settingsun.order.service.impl;


import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wonu.settingsun.common.exception.BmsException;
import com.wonu.settingsun.common.utils.DateUtils;
import com.wonu.settingsun.common.utils.RedisUtils;
import com.wonu.settingsun.doctor.dao.ScheduleDao;
import com.wonu.settingsun.domain.dto.AppointmentQueryDto;
import com.wonu.settingsun.domain.entity.AppointmentOrder;

import com.wonu.settingsun.domain.entity.DoctorSchedule;
import com.wonu.settingsun.domain.vo.AppointmentOrderVO;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.mq.MQKey;
import com.wonu.settingsun.mq.MqUtils;

import com.wonu.settingsun.order.dao.AppointmentDao;
import com.wonu.settingsun.order.dao.AppointmentVoDao;
import com.wonu.settingsun.order.service.AppointmentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;

import org.springframework.scheduling.TaskScheduler;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AppointmentServiceImpl
 * @date 2023/11/9 17:07
 * @description
 */

@Slf4j
@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Resource
    private AppointmentDao appointmentDao;
    @Resource
    private AppointmentVoDao appointmentVoDao;
    @Resource
    private ScheduleDao scheduleDao;
    @Resource
    private MqUtils mqUtils;
    @Resource
    private RedisUtils redisUtils;

    @Resource
    private TaskScheduler taskSchedule;

    private long insertId;

    /**
     * @param currentPage
     * @param pageSize
     * @param appointmentQueryDto
     * @return PageVo<List>
     * @author nameless
     * @date 2023/11/10 10:50
     * @description 多条件分页查询视图
     */
    @Override
    public PageVo<List> findAllByPage(int currentPage, int pageSize, AppointmentQueryDto appointmentQueryDto) {

        IPage<AppointmentOrderVO> page = new Page<>();
        page.setCurrent(currentPage);
        page.setSize(pageSize);

        Integer status = appointmentQueryDto.getStatus();
        Date time = appointmentQueryDto.getAppointmentTime();
        String doctorName = appointmentQueryDto.getDoctorName();
        String patientName = appointmentQueryDto.getPatientName();

        QueryWrapper<AppointmentOrderVO> wrapper = new QueryWrapper<>();
        wrapper.eq(Objects.nonNull(status), "order_status", status)
                .le(Objects.nonNull(time), "appointment_time", time)
                .like(StringUtils.hasText(doctorName), "doctor", doctorName)
                .like(StringUtils.hasText(patientName), "patient", patientName);
        IPage<AppointmentOrderVO> orderVOIPage = appointmentVoDao.selectPage(page, wrapper);

        PageVo<List> pageVo = new PageVo<>();
        pageVo.setTotal(orderVOIPage.getTotal());
        pageVo.setData(orderVOIPage.getRecords());

        return pageVo;
    }

    @Override
    public AppointmentOrder getById(int id) {

        return appointmentDao.selectById(id);
    }


    /**
     * @param username
     * @return AppointmentOrder
     * @author nameless
     * @date 2023/11/10 17:27
     * @description 获取当前医师的支付完成的订单, 按照升序排序
     */
    @Override
    public List<AppointmentOrder> findAppointmentByDoctorName(String username) {

        List<AppointmentOrder> orders = appointmentDao.selectList(new QueryWrapper<AppointmentOrder>()
                .eq("doctor_name", username)
                .eq("order_status", 2)
                .orderByAsc("create_time"));

        return orders;
    }

    /**
     * @param appointmentOrder 无id，status
     * @return AppointmentOrder
     * @author nameless
     * @date 2023/11/10 14:36
     * @description 根据表单传递的参数判断是否已经预约过，存在1，2状态的预约记录则不允许再预约
     */
    @Override
    public boolean hasOrder(AppointmentOrder appointmentOrder) {

        QueryWrapper<AppointmentOrder> wrapper = new QueryWrapper<>();
        Date time = appointmentOrder.getAppointmentTime();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String format = sdf.format(time);
        String patientName = appointmentOrder.getPatientName();
        String doctorName = appointmentOrder.getDoctorName();

        wrapper.eq(Objects.nonNull(time), "appointment_time", format)
                .eq(StringUtils.hasText(patientName), "patient_name", patientName)
                .eq(StringUtils.hasText(doctorName), "doctor_name", doctorName)
                .in("order_status", 1, 2);
        List<AppointmentOrder> orders = appointmentDao.selectList(wrapper);

        return orders.size() > 0;
    }

    /**
     * @param appointmentOrder
     * @return boolean
     * @author nameless
     * @date 2023/11/9 21:46
     * @description 创建预约订单前进入mq里进行排队, 即对同一个医师的预约需要先来后到进行分配
     */
    @Override
    public void createAppointment(AppointmentOrder appointmentOrder) {

        LocalTime time = LocalTime.now();

        LocalTime start = LocalTime.of(10, 0, 0);
        LocalTime end = LocalTime.of(20, 0, 0);
        if (time.isBefore(start) || time.isAfter(end)) throw new BmsException(500, "未到预约时间");


        if (hasOrder(appointmentOrder)) throw new BmsException(500, "当天你已经预约过，请查看预约记录");

        log.debug("日志测试");
        appointmentOrder.setOrderStatus(1);
        //设置默认金额
        appointmentOrder.setCost(5.00);
        appointmentOrder.setCreateTime(new Date());

        String jsonStr = JSONUtil.toJsonStr(appointmentOrder);

        //进入mq进行排队
        mqUtils.routingProduce(jsonStr, MQKey.MODULE_EXCHANGE.getName(), MQKey.ORDER.toString());

    }

    /**
     * @param jsonStr
     * @return void
     * @author nameless
     * @date 2023/11/9 22:22
     * @description 消费者监听队列，根据传值查询redis并自减，满足则让创建，不满足则抛出异常；
     * 同时因为需要将插入数据库中的记录id返回，因为需要使用事件的方法进行回调（mq的回调有待学习）
     * 由于此处使用了mq进行排队，因为消费中的方法是可以避免同时到达多个请求，避免了竞态条件。
     */
    @RabbitListener(queues = "settingSun_order_queue")
    public void consumeAppointment(String jsonStr) {

        AppointmentOrder appointmentOrder = JSONUtil.toBean(jsonStr, AppointmentOrder.class);
        Date time = appointmentOrder.getAppointmentTime();
        String name = appointmentOrder.getDoctorName();
        String day = String.valueOf(DateUtils.dayOfWeek(time));

        //获取redis中的当天医师的剩余名额
        try {
        Integer value = (Integer) redisUtils.hget(day, name);


            if (value > 0) {
                //付款后再减数据库，同时变更订单状态，此时仅减redis中名额
                redisUtils.descHValue(day, name);

                int insert = appointmentDao.insert(appointmentOrder);
                if (insert > 0) {
                    insertId = appointmentOrder.getAppointmentId();
                    //发布事件, 事件负责传递id数据
                    /* orderEventPublisher.publishOrderCreatedEvent(id);*/

             /* appointmentTask.setOrderId(insertId);
                appointmentTask.countdown();*/
                    startTimerTask();

                }
            } else {
                //不能够抛出异常
                throw new BmsException(500, "预约失败，当前医师预约时间内人数已满，请重新选择");
            }
        } catch (BmsException exception) {
            log.debug(exception.getMessage());
        }

    }


    /**
     * @param appointmentOrder 是前端传递的完整一条记录
     * @return boolean
     * @author nameless
     * @date 2023/11/10 10:50
     * @description 修改订单的状态, 取消0，或者支付完成2 3是处理完，之后进行更新排班表（先修改可以避免非法订单一样更新排班表）
     */
    @Transactional(isolation = Isolation.REPEATABLE_READ)
    public void modifyAppointmentStatus(AppointmentOrder appointmentOrder) {

        Integer status = appointmentOrder.getOrderStatus();
        AppointmentOrder result = appointmentDao.selectById(appointmentOrder.getAppointmentId());
        if (result.getOrderStatus().equals(status)) {
            throw new BmsException(500, "修改状态无效");
        }

        int row = appointmentDao.updateById(appointmentOrder);
        if (row <= 0) throw new BmsException(500, "非法订单，修改失败");

        Date time = appointmentOrder.getAppointmentTime();
        String day = String.valueOf(DateUtils.dayOfWeek(time));
        String doctorName = appointmentOrder.getDoctorName();
        // 支付订单，更新排班数据库，支付一个减1
        if (status == 2) {
            DoctorSchedule doctorSchedule = scheduleDao
                    .selectOne(new QueryWrapper<DoctorSchedule>()
                            .eq("doctor_name", doctorName)
                            .eq("day_of_week", day));
            doctorSchedule.setWorkload(doctorSchedule.getWorkload() - 1);
            scheduleDao.updateById(doctorSchedule);

        } else if (status == 0) {
            //超时、取消 redis+1   反馈信息
            redisUtils.incrHValue(day, doctorName);
            throw new BmsException(500, "订单已取消");
        } else if (status == 3) {
            //处理完成订单（看病） 将排班表+1
            DoctorSchedule doctorSchedule = scheduleDao
                    .selectOne(new QueryWrapper<DoctorSchedule>()
                            .eq("doctor_name", doctorName)
                            .eq("day_of_week", day));
            doctorSchedule.setWorkload(doctorSchedule.getWorkload() + 1);
            scheduleDao.updateById(doctorSchedule);
            redisUtils.incrHValue(day, doctorName);
        }
    }

    @Override
    public void modifyAppointment(AppointmentOrder appointmentOrder) {

        appointmentDao.updateById(appointmentOrder);

    }

    /**
     * @param appointmentOrder
     * @return void
     * @author nameless
     * @date 2023/11/10 10:57
     * @description 删除订单，主要用于定时删除数据库中的取消状态订单，当然也可以使用id单个删除
     */
    @Override
    public void removeAppointment(AppointmentOrder appointmentOrder) {

        QueryWrapper<AppointmentOrder> wrapper = new QueryWrapper<>();

        Long id = appointmentOrder.getAppointmentId();
        Integer orderStatus = appointmentOrder.getOrderStatus();
        wrapper.eq(Objects.nonNull(id), "appointment_id", id)
                .eq(Objects.nonNull(orderStatus), "order_status", orderStatus);

        int row = appointmentDao.delete(wrapper);

        if (row <= 0) {
            throw new BmsException(500, "删除预约订单失败");
        }
    }
    
    
    /**
     * @author nameless
     * @date 2023/11/15 10:31
     * @param 
     * @return void
     * @description 手动定时
     */
    private void startTimerTask() {
        // 创建一个实现 Runnable 接口的定时任务
        Runnable timerTask = () -> {
            // 定时任务逻辑，处理订单
            if (Objects.isNull(insertId)) throw new BmsException(500, "insertId参数为空");
            AppointmentOrder appointmentOrder = appointmentDao.selectById(insertId);
            if (Objects.isNull(appointmentOrder)) throw new BmsException(500, "需要自动取消的订单不存在");

            Integer status = appointmentOrder.getOrderStatus();
            if (status == 1) {
                appointmentOrder.setOrderStatus(0);
                appointmentDao.updateById(appointmentOrder);
                throw new BmsException(500, "订单支付超时，已自动取消");
            }
            System.out.println("定时任务执行：处理订单逻辑");
        };

        // 触发时间，例如设置为订单创建后的1分钟
        Date triggerTime = new Date(System.currentTimeMillis() + 1000 * 60 * 1);

        // 启动定时任务
        taskSchedule.schedule(timerTask, triggerTime);
    }
}