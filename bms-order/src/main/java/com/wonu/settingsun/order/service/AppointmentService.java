package com.wonu.settingsun.order.service;

import com.wonu.settingsun.domain.dto.AppointmentQueryDto;
import com.wonu.settingsun.domain.entity.AppointmentOrder;
import com.wonu.settingsun.domain.vo.PageVo;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AppointmentService
 * @date 2023/11/9 17:07
 * @description  预约门诊订单
 */
public interface AppointmentService {

    PageVo<List> findAllByPage(int currentPage, int pageSize, AppointmentQueryDto appointmentQueryDto);

    AppointmentOrder getById(int id);

    List<AppointmentOrder> findAppointmentByDoctorName(String username);

    boolean hasOrder(AppointmentOrder appointmentOrder);

    void createAppointment(AppointmentOrder appointmentOrder);

    void modifyAppointmentStatus(AppointmentOrder appointmentOrder);
    void modifyAppointment(AppointmentOrder appointmentOrder);

    void removeAppointment(AppointmentOrder appointmentOrder);

}
