package com.wonu.settingsun.order.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * @author nameless
 * @version 1.0
 * @ClassName OrderScheduleConfig
 * @date 2023/11/14 20:26
 * @description  定时器配置类
 */
@Configuration
@EnableScheduling
public class OrderScheduleConfig {

    /**
     * @author nameless
     * @date 2023/11/14 20:27
     * @param
     * @return TaskScheduler
     * @description 手动启动定时任务
     */
    @Bean
    public TaskScheduler taskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

}