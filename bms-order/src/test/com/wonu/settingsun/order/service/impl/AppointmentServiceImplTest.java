package com.wonu.settingsun.order.service.impl;
import java.util.Calendar;
import java.util.Date;

import com.wonu.settingsun.domain.entity.AppointmentOrder;
import com.wonu.settingsun.order.service.AppointmentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName AppointmentServiceImplTest
 * @date 2023/11/10 11:04
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class AppointmentServiceImplTest {

    @Resource
    private AppointmentService appointmentService;

    @Test
    public void findAllByPage() {
    }

    @Test
    public void createAppointment() {
        AppointmentOrder appointmentOrder = new AppointmentOrder();

        appointmentOrder.setDoctorName("doc043");
        appointmentOrder.setPatientName("sun014");
        appointmentOrder.setContent("救救我");
        Calendar instance = Calendar.getInstance();
        instance.add(Calendar.DATE, 1);
        Date tomorrow = instance.getTime();
        appointmentOrder.setAppointmentTime(tomorrow);
        appointmentOrder.setCreateTime(new Date());


        appointmentService.createAppointment(appointmentOrder);

    }

    @Test
    public void modifyAppointment() {

        AppointmentOrder appointmentOrder = appointmentService.getById(7);
        appointmentOrder.setOrderStatus(3);
        appointmentService.modifyAppointmentStatus(appointmentOrder);

    }

    @Test
    public void removeAppointment() {
    }


    @Test
    public void addtest() {

    }
}