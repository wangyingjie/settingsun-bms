package com.wonu.settingsun.logs.controller;

import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.domain.dto.LogQueryDto;
import com.wonu.settingsun.domain.entity.SysLogOpt;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.logs.service.LogService;
import io.swagger.annotations.Api;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogController
 * @date 2023/11/9 10:59
 * @description
 */
@Api(tags = "日志模块")
@RestController
@RequestMapping("/api/log")
@Validated
public class LogController {

    @Resource
    private LogService logService;



    @GetMapping("findAll")
    public HttpResp<PageVo<List>> findAllByPage(@RequestBody LogQueryDto logQueryDto, @NotNull int pageNum, @NotNull int pageSize) {

        PageVo<List> allByPage = logService.findAllByPage(pageNum, pageSize, logQueryDto);

        return HttpResp.success(RespCode.OK, allByPage);
    }

}