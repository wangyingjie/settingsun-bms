package com.wonu.settingsun.logs.service;

import com.wonu.settingsun.domain.dto.LogQueryDto;
import com.wonu.settingsun.domain.entity.SysLogOpt;
import com.wonu.settingsun.domain.vo.PageVo;

import java.util.Date;
import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogService
 * @date 2023/11/8 20:47
 * @description
 */
public interface LogService {

    PageVo<List> findAllByPage(int currentPage, int pageSize, LogQueryDto logQueryDto);



    void addLog(SysLogOpt sysLogOpt);

    void removeLog(Long logId);

    void logToExcel(Date date);
}