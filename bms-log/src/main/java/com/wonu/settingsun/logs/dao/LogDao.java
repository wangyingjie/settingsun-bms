package com.wonu.settingsun.logs.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.SysLogOpt;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogDao
 * @date 2023/11/8 21:22
 * @description
 */
@Mapper
public interface LogDao extends BaseMapper<SysLogOpt> {

}
