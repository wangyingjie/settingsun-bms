package com.wonu.settingsun.logs;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogApp
 * @date 2023/11/9 10:58
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = "com.wonu.settingsun")
public class LogApp {
    public static void main(String[] args) {
        SpringApplication.run(LogApp.class, args);
    }
}