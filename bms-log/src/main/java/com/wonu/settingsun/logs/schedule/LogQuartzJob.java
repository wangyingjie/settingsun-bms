package com.wonu.settingsun.logs.schedule;

import com.wonu.settingsun.logs.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

import javax.annotation.Resource;
import java.util.Calendar;

/**
 * @author nameless
 * @version 1.0
 * @ClassName logQuartzJob
 * @date 2023/11/14 12:01
 * @description   日志定时任务，每一天将数据库中的日志记录导出到excel表中(取消)
 */
@Slf4j
public class LogQuartzJob extends QuartzJobBean {

    @Resource
    private LogService logService;


    @Override
    protected void executeInternal(JobExecutionContext context) throws JobExecutionException {


    }
}