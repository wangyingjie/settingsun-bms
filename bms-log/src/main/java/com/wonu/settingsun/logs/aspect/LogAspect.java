package com.wonu.settingsun.logs.aspect;

import cn.hutool.json.JSONObject;
import cn.hutool.json.JSONUtil;
import com.auth0.jwt.JWT;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.wonu.settingsun.domain.entity.SysLogOpt;
import com.wonu.settingsun.logs.annotation.SysLog;
import com.wonu.settingsun.logs.service.LogService;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.Date;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogAspect
 * @date 2023/11/8 19:54
 * @description 日志切面类, 对特定的注解进行增强
 */
@Component
@Aspect
@Slf4j
public class LogAspect {

    @Resource
    private LogService logService;

    //定义切点 ， 对于所有被SysLog注解标注的方法
    @Pointcut("@annotation(com.wonu.settingsun.logs.annotation.SysLog)")
    public void logPointCut() {

    }

    @Around("logPointCut()")
    public Object aroundLogPointCut(ProceedingJoinPoint joinPoint) {

        System.out.println("前置通知");
        long startTime = System.currentTimeMillis();
        Object result = null;
        try {
             result = joinPoint.proceed();
        } catch (Throwable e) {
            //异常通知
            throw new RuntimeException("环绕通知异常");
        }
        long endTime = System.currentTimeMillis();

        insertLog(joinPoint, endTime - startTime, result);

        //后置通知
        return result;


    }

    private void insertLog(ProceedingJoinPoint joinPoint, long time, Object result)  {

        //获取连接对象
        ServletRequestAttributes requestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = requestAttributes.getRequest();

        //获取发起连接的用户

        String url = request.getRequestURL().toString();
        String uri = request.getRequestURI();
        // 获取请求方式
        String requestType = request.getMethod();
        //获取方法签名得到注解内容、当前方法
        Signature signature = joinPoint.getSignature();
        MethodSignature methodSignature = (MethodSignature) signature;
        Method method = methodSignature.getMethod();
        SysLog annotation = method.getAnnotation(SysLog.class);
        String describe = annotation.value();
        String methodName = method.getName();

        //获取请求参数
        String args = Arrays.toString(joinPoint.getArgs());

        // 把方法返回的Result转换为json; 使用默认的对象与json的转换工具包
        ObjectMapper objectMapper = new ObjectMapper();
        String resultTOJson = null;
        try {
            resultTOJson = objectMapper.writeValueAsString(result);
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }

        String jwt = "";
        if(methodName.equals("login")) {
            JSONObject entries = JSONUtil.parseObj(resultTOJson);
            jwt = entries.getStr("data");
        }else{
            jwt = request.getHeader("token");
        }
        //获取发起连接的用户
        String username = JWT.decode(jwt).getClaim("username").asString();

        SysLogOpt sysLogOpt = new SysLogOpt();

        sysLogOpt.setReqType(requestType);
        sysLogOpt.setReqUrl(url);
        sysLogOpt.setReqUri(uri);
        sysLogOpt.setOperator(username);
        sysLogOpt.setMethod(methodName);
        sysLogOpt.setOptDesc(describe);
        sysLogOpt.setReqParams(args);
        sysLogOpt.setRespData(resultTOJson);
        sysLogOpt.setOptTime(new Date());
        sysLogOpt.setSpendTime(time);

        logService.addLog(sysLogOpt);
    }


}