package com.wonu.settingsun.logs.config;

import cn.hutool.log.Log;
import com.wonu.settingsun.common.result.Const;
import com.wonu.settingsun.logs.schedule.LogQuartzJob;
import org.quartz.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author nameless
 * @version 1.0
 * @ClassName quartzConfig
 * @date 2023/11/14 12:03
 * @description
 */
@Configuration
public class QuartzConfig {
    //创建任务细节
    @Bean
    public JobDetail jobDetail() {
        return JobBuilder.newJob(LogQuartzJob.class)
                .storeDurably(true)  //使持久存在
                .build();
    }

    //创建触发器
    @Bean
    public Trigger trigger() {
        return TriggerBuilder.newTrigger().forJob(jobDetail())//绑定任务细节
                .withSchedule(CronScheduleBuilder.cronSchedule(Const.LOGSCHEDULETIME)) //定时时间
                .build();
    }
}