package com.wonu.settingsun.logs.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.plugins.pagination.PageDTO;
import com.wonu.settingsun.domain.dto.LogQueryDto;
import com.wonu.settingsun.domain.entity.SysLogOpt;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.logs.dao.LogDao;
import com.wonu.settingsun.logs.service.LogService;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName LogServiceImpl
 * @date 2023/11/8 20:48
 * @description
 */
@Service
public class LogServiceImpl implements LogService {

    @Resource
    private LogDao logDao;

    @Override
    public PageVo<List> findAllByPage(int currentPage, int pageSize, LogQueryDto logQueryDto) {

        //定义page对象
        IPage<SysLogOpt> page = new Page();
        page.setCurrent(currentPage);
        page.setSize(pageSize);

        String username = logQueryDto.getUsername();
        Date optTime = logQueryDto.getOptTime();
        Long spendTime = logQueryDto.getSpendTime();

        //定义选择器
        QueryWrapper<SysLogOpt> wrapper = new QueryWrapper<>();
        wrapper.like(StringUtils.hasText(username), "operator", "%" + username + "%")
                .le(Objects.nonNull(optTime), "opt_time", optTime)
                .ge(Objects.nonNull(spendTime), "spend_time",spendTime);

        IPage<SysLogOpt> sysLogOptIPage = logDao.selectPage(page, wrapper);



        PageVo<List> listPageVo = new PageVo<>();
        listPageVo.setData(sysLogOptIPage.getRecords());
        listPageVo.setTotal(sysLogOptIPage.getTotal());

        return listPageVo;
    }

    @Override
    public void addLog(SysLogOpt sysLogOpt) {

        logDao.insert(sysLogOpt);
    }

    @Override
    public void removeLog(Long logId) {

        logDao.deleteById(logId);
    }

    /**
     * @author nameless
     * @date 2023/11/14 12:17
     * @param date
     * @return void
     * @description 获取一天内的所有操作日志，并存储到excel中
     */
    @Override
    public void logToExcel(Date date) {

        List<SysLogOpt> sysLogOpts = logDao.selectList(new QueryWrapper<SysLogOpt>().eq("opt_time", date));
        System.out.println(sysLogOpts.toString());

    }
}