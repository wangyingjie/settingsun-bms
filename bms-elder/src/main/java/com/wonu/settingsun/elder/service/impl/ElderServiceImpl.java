package com.wonu.settingsun.elder.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.wonu.settingsun.common.exception.BmsException;
import com.wonu.settingsun.common.utils.StringValidateUtils;
import com.wonu.settingsun.domain.entity.ElderlyHealthInfo;
import com.wonu.settingsun.domain.dto.ElderQueryDto;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.elder.dao.ElderDao;
import com.wonu.settingsun.elder.service.ElderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;
import java.util.Objects;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ElderServiceImpl
 * @date 2023/11/6 19:16
 * @description
 */
@Service
public class ElderServiceImpl implements ElderService {

    @Resource
    private ElderDao elderDao;

    @Override
    public List<ElderlyHealthInfo> findAll() {

        return elderDao.selectList(null);
    }

    /**
     * @author nameless
     * @date 2023/11/6 19:41
     * @param currentPage
     * @param pageSize
     * @return List<ElderlyHealthInfo>
     * @description 分页拦截器显示指定的当前页和页面大小的记录
     */
    @Override
    public PageVo<List<ElderlyHealthInfo>> findInfosByPage(int currentPage, int pageSize, ElderQueryDto elderQueryDto) {

        IPage<ElderlyHealthInfo> page = new Page<>();
        page.setCurrent(currentPage);
        page.setSize(pageSize);

        Integer gender = elderQueryDto.getGender();
        String name = elderQueryDto.getName();
        String username = elderQueryDto.getUsername();

        QueryWrapper<ElderlyHealthInfo> wrapper = new QueryWrapper<>();
        wrapper.eq(Objects.nonNull(gender),"gender", gender)
                .like(StringValidateUtils.hasText(name), "name","%" + name +"%")
                .like(StringValidateUtils.hasText(username), "username","%" + username +"%");
        IPage<ElderlyHealthInfo> iPage = elderDao.selectPage(page, wrapper);

        PageVo<List<ElderlyHealthInfo>> pageVo = new PageVo<>();
        pageVo.setData(iPage.getRecords());
        pageVo.setTotal(iPage.getTotal());
        //返回当前页的记录数
        return pageVo;
    }

    /**
     * @author nameless
     * @date 2023/11/7 21:25
     * @param info
     * @return void
     * @description 添加健康信息
     */
    @Override
    public boolean addHealthInfo(ElderlyHealthInfo info) {

        info.setCreateTime(new Date(System.currentTimeMillis()));
        int row = elderDao.insert(info);
        return row > 0;
    }

    @Override
    public boolean modifyHealthInfo(ElderlyHealthInfo info) {

        info.setUpdateTime(new Date(System.currentTimeMillis()));
        //动态修改，传入的参数有修改的就改，没修改的不变
        int row = elderDao.update(info, new QueryWrapper<ElderlyHealthInfo>().eq("username", info.getUsername()));
        return row > 0;
    }

    @Override
    public boolean removeHealthInfo(Long id) {
        int row = elderDao.deleteById(id);
        return row > 0;
    }

    @Override
    @Transactional
    public void modifyHealthInfos(List<Long> ids) {
        int rows = elderDao.deleteBatchIds(ids);
        if(rows != ids.size()) {
            throw new BmsException(400, "批量删除失败");
        }
    }
}