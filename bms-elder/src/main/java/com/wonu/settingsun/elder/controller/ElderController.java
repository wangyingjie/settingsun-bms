package com.wonu.settingsun.elder.controller;

import com.wonu.settingsun.common.exception.BmsException;
import com.wonu.settingsun.common.result.HttpResp;
import com.wonu.settingsun.common.result.RespCode;
import com.wonu.settingsun.domain.entity.ElderlyHealthInfo;
import com.wonu.settingsun.domain.dto.ElderQueryDto;
import com.wonu.settingsun.domain.vo.PageVo;
import com.wonu.settingsun.elder.service.ElderService;
import com.wonu.settingsun.logs.annotation.SysLog;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.constraints.NotNull;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ElderController
 * @date 2023/11/7 17:07
 * @description  老人信息管理处理类
 */
@RestController
@Api(tags = "健康信息管理")
@RequestMapping("/api/elderInfo")
@Validated
public class ElderController {

    @Resource
    private ElderService elderService;

    @ApiOperation(value = "多条件分页查询", notes = "findInfosByPage")
    @PostMapping("page")
    public HttpResp<PageVo<List<ElderlyHealthInfo>>> findInfosByPage(@RequestBody ElderQueryDto elderQueryDto, int pageNum, int pageSize) {

        PageVo<List<ElderlyHealthInfo>> pageVo = elderService.findInfosByPage(pageNum, pageSize, elderQueryDto);


        return HttpResp.success(RespCode.OK, pageVo);
    }


    @SysLog("修改健康信息")
    @ApiOperation(value = "修改健康信息记录", notes = "modifyInfo")
    @PutMapping("/modifyInfo")
    public HttpResp modifyInfo(@Validated @RequestBody ElderlyHealthInfo elderlyHealthInfo) {


        return HttpResp.isSuccess(elderService.modifyHealthInfo(elderlyHealthInfo),"修改健康信息");
    }

    @SysLog("添加健康信息")
    @ApiOperation(value = "添加健康信息记录", notes = "addInfo")
    @PostMapping("/addInfo")
    public HttpResp addInfo(@Validated @RequestBody ElderlyHealthInfo elderlyHealthInfo) {


        return HttpResp.isSuccess( elderService.addHealthInfo(elderlyHealthInfo),"添加健康信息");
    }

    @SysLog("删除健康信息")
    @ApiOperation(value = "删除单条记录", notes = "removeById")
    @DeleteMapping("/removeById/{id}")
    public HttpResp removeById(@NotNull(message = "id不能为空") @PathVariable("id") Long id) {

        if(!elderService.removeHealthInfo(id)) throw new BmsException(500, "删除的用户id不存在");

        return HttpResp.success(200,"删除成功", null);
    }

    @SysLog("批量删除健康信息")
    @ApiOperation(value = "批量删除",notes = "removeByBatch")
    @DeleteMapping("/removeByBatch")
    public HttpResp removeByBatch(@RequestBody String[] ids) {

        if(ids.length <= 0) {
            throw new BmsException(400, "所选数组为空");
        }
        List<Long> longIds = Arrays.stream(ids).map(Long::parseLong).collect(Collectors.toList());
        elderService.modifyHealthInfos(longIds);
        return HttpResp.success(RespCode.OK, null);
    }
}