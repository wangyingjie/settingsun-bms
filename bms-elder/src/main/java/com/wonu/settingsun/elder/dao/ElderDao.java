package com.wonu.settingsun.elder.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.wonu.settingsun.domain.entity.ElderlyHealthInfo;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ElderDao
 * @date 2023/11/6 19:18
 * @description
 */
@Mapper
public interface ElderDao extends BaseMapper<ElderlyHealthInfo> {
}
