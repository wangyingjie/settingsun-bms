package com.wonu.settingsun.elder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ElderApp
 * @date 2023/11/6 19:20
 * @description
 */
@SpringBootApplication
@ComponentScan(basePackages = {"com.wonu.settingsun.elder","com.wonu.settingsun.common"})
@EnableTransactionManagement//开启Spring事务管理
public class ElderApp {

    public static void main(String[] args) {
        SpringApplication.run(ElderApp.class,args);
    }
}