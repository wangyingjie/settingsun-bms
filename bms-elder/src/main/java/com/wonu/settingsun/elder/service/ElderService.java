package com.wonu.settingsun.elder.service;

import com.wonu.settingsun.domain.entity.ElderlyHealthInfo;
import com.wonu.settingsun.domain.dto.ElderQueryDto;
import com.wonu.settingsun.domain.vo.PageVo;

import java.util.List;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ElderService
 * @date 2023/11/6 18:59
 * @description
 */
public interface ElderService {

    List<ElderlyHealthInfo> findAll();

    PageVo<List<ElderlyHealthInfo>> findInfosByPage(int currentPage, int pageSize, ElderQueryDto elderQueryDto);

    boolean addHealthInfo(ElderlyHealthInfo info);

    boolean modifyHealthInfo(ElderlyHealthInfo info);

    boolean removeHealthInfo(Long id);

    void modifyHealthInfos(List<Long> ids);

}
