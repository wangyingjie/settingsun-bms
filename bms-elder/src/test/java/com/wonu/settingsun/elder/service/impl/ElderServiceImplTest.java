package com.wonu.settingsun.elder.service.impl;

import com.wonu.settingsun.elder.service.ElderService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;

import static org.junit.Assert.*;

/**
 * @author nameless
 * @version 1.0
 * @ClassName ElderServiceImplTest
 * @date 2023/11/6 19:18
 * @description
 */
@SpringBootTest
@RunWith(SpringRunner.class)
public class ElderServiceImplTest {

    @Resource
    private ElderService elderService;

    @Test
    public void findAll() {
        elderService.findAll();
    }

    @Test
    public void findInfosByPage() {

    }
}